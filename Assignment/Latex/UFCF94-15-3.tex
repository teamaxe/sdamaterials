\documentclass[a4paper,11pt,oneside]{article}
\input{standardheader.tex}

%\usepackage{draftwatermark}
%\SetWatermarkText{Draft}
%\SetWatermarkScale{2}

\begin{document}
\parindent=0pt
\parskip=.4cm
\begin{center}
{\LARGE Software Development for Audio (UFCF94-15-3) \\
Assignment and Presentation Specification
}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Important Info and Dates}
\begin{itemize}
\item The assessment for SDA includes a presentation (25\% of the module) and an assignment (75\% of the module).
\item The presentation will take place on \textbf{Thursday 21st January 2021}.
\item The submission deadline for the assignment is \textbf{Thursday 11th February 2021 at 14.00} (Blackboard submission).
\item Both the assignment and presentation are described below and must be completed individually.
\end{itemize}

\section{Assessment Brief}
You are required to develop an audio application for Windows or Mac OSX. You are free to decide on the function of the application but it must perform an audio and/or MIDI process and have a graphical user interface (GUI) for visualisation and control. The application must be written in C++ and use the Juce library (other supporting libraries may be used as appropriate). 

The assessment for the module consists of a presentation and assignment, both relating to your application. The presentation will take place first and will involve a verbal introduction, prototype demonstration and a short Q\&A session; see section \ref{sec::presentation} for more details. The assignment with involve submitting the software (application $+$ source code) and a report, see section \ref{sec::assignment} for more details including the requirements of the application. 


%===============================================================================================

\section{The Presentation (25\% of module)}
\label{sec::presentation}
The presentation will take place remotely using a web conferencing platform and will provide an opportunity for you to convey your understanding of the taught material and demonstrate the design and implementation of your prototype audio application (see section \ref{sec::assignment}). The presentation must include (class relationship/block) diagrams to support the communication of your design. You may also use presentation slides but this is not a requirement.

The presentation will last up to 15 minutes and will consist of three parts:\\
\begin{tabular}{l p{17cm} }
1	& \textbf{Introduction and Demonstration ($\sim$5 mins)} Including the assignment aims, rationale, demonstration/evaluation of the prototype application and plans for the remaining period.\\
2 	& \textbf{Software Architecture ($\sim$5 mins)} Presenting a high-level overview of the architecture communicating the structure and relationships between classes that make up the system along with plans for future developments. This must include class relationship diagrams representing the current and future system.\\
3	& \textbf{Feedback and Future Planning ($\sim$5 mins)} Feedback on the software will be provided with recommendations to support future assignment developments.\\
\end{tabular}
\newpage
\subsection{Assessment/Grading Criteria}
The marks for the presentation will be allocated as follows:

\begin{enumerate}
\item \textbf{Demonstration of Prototype, System Architecture and Project Planning} (75 marks)\\
\begin{tabular}{l p{14cm} }
0-29 	& The presented work does not represent an adequate prototype for the assignment. A diagrammatic representation of the system nor coherent plan for future developments is presented.\\
30-37   & The presented prototype does not develop the practical work. A vague diagrammatic representation of the software structure is presented with little reflection/evaluation and minimal plans for future developments. A conceptual understanding of the module content is not evident from the work nor the presentation.\\
38-44 	& The system represents a prototype of the intended system in which relevant work from the practicals have been extracted and synthesised into a clear prototype for the intended system. The diagrammatic representation of the current architecture is clear although future developments are less formed. The presenter conveys a basic appreciation of the conceptual elements of the module material.\\
45-52	& The system represents an ambitious prototype with creative ideas that demonstrate extensions to the practical exercises completed so far. A considered plan for future developments has been established and are well represented diagrammatically for the current phase of the assignment with considerations for future developments. A clear understanding of the conceptual content of the module is evident.\\
53-75	& The prototype exceeds expectations with an exceptional diagrammatic system representation for the current developments with outstanding future plans and milestones.\\
\end{tabular}
\item \textbf{Presentation Style and Preparation} (25 marks)\\
\begin{tabular}{l p{14cm} }
0-9 		& Presenter is not adequately prepared and fails to communicate the requirements of the assignment at this stage.\\
10-12    	& Presenter is prepared although unrehearsed with unexpected problems restricting the communication of the work at this stage. Prompting is required for the assessors to sufficiently assess the assignment progress and responses to questions are inadequate.\\
13-15  	& Presenter is well prepared, organised and rehearsed communicating the work well although elements of the presentation could have been clearer. Responses to questions convey a good understanding of the relevant module material.\\
16-18	& Presentation is smooth and well rehearsed with clear and logical presentation of ideas. A clear appreciation of the assessment requirements is demonstrated and potential issues have been identified and questions formed to support future developments. Presenter demonstrates a clear awareness of the module material in their responses to questions and suggestions.\\
19-25	& Presentation is exceptionally organised and effectively executed, building on all positive criteria outlined above. \\
\end{tabular}
\end{enumerate}

\newpage
\textbf{General Feedback from Last Year's Presentations}\\
For the presentation, overall those who received the highest marks:

\begin{tabular}{l p{17cm} }
$\bullet$	& were well prepared and rehearsed.\\
$\bullet$ 	& conducted themselves in professional manor.\\
$\bullet$	& demonstrated the features of their application with little prompting.\\
$\bullet$	& provided an organised and fluid description of their system.\\
$\bullet$	& made good use of a clear class relationship diagram.\\
$\bullet$	& clearly explained the structure and connections between the software components.\\
$\bullet$	& responded to questions using the appropriate terminology.\\
$\bullet$	& made good use of the available time.\\
$\bullet$ 	& were able to evaluate their work critically and suggest practical ideas for improvement.
\end{tabular}

%=================================================================================================================
\section{The Assignment (75\% of module)}
\label{sec::assignment}
The assignment enables you to demonstrate your understanding of software development best practices in context at the same time providing an opportunity for you to appreciate and refine your approach to the application development life-cycle on a greater scale than exercises completed in the lab. 

\subsection{Features and Requirements}
Your audio application must meet the following \textbf{minimum} features and requirements:\\
\begin{tabular}{l p{17cm} }
$\bullet$	& The application must have \textbf{at least one} audio/MIDI input or output.\\
$\bullet$	& The application must include GUI components through which the application is controlled and/or activity is visualised.\\
$\bullet$	& The source code must include \textbf{a minimum} of three classes written by you.  \\
$\bullet$	& The source code must be neatly structured with computation divided into classes and functions.\\
$\bullet$ 	& The source code must be well documented with Doxygen comments.\\
$\bullet$	& The source code must be maintained via a Git repository. \\
$\bullet$	& The source code must be neatly presented with a consistent style and with a consistent naming convention. \\
\end{tabular}\\

The list above represents the minimum requirements of the \textbf{assignment}, you are strongly encouraged to be imaginative and create an application that extends beyond these guidelines. 

\subsection{Submitted Material (Blackboard Submission)}
The assignment must be submitted to blackboard in the form of a single .zip file containing:\\
\begin{tabular}{l p{17cm} }
1. 	& A written report in a common file format (e.g. pdf, word)\\
2. 	& Your full Git assignment repository (including project files, sourcode, HTML Doxygen output, excluding intermediate Build files)\\
2. 	& A compiled application executable\\
\end{tabular}

\begin{center}
\textbf{Ensure that you allow sufficient time to upload the files}
\end{center}
\newpage
\subsection{Written Report}
You must submit a written report no longer than 1000 words (excluding the Doxygen generated appendix) as a single coherent document written in the third person with cover/contents pages, section/page numbers, illustrations and structured as follows:

\begin{enumerate}
\item \textbf{Introduction.}
Introduce the context, outline your motivation for developing the application and provide a brief summary of the following sections of the report.
\item \textbf{User Manual.}
Introduce your application and describe how it is used in the style of a manual intended to be read by end-users. Include an image of the GUI and an overview of the features. Do not discuss the underlying code (use system documentation for this). 
\item \textbf{System Documentation.}
Enable developers with similar expertise to yourself to understand the structure of the application so that they could continue its development. Do not discuss step-by-step code details (use in-line comments in the source code for this). \textbf{You must include:}\\
\begin{tabular}{l p{15cm} }
(a)	& A class relationship diagram showing the dependencies between the classes of the system.\\
(b)	& An overview of the system structure which may take the form of a block diagram.\\
\end{tabular}
\item \textbf{Conclusions.}
Write a reflective commentary on the completed work (first person narrative is permissible here!). You may wish to discuss any of the following:\\
\begin{tabular}{l p{15cm} }
(a) & Your achievements and learning experiences. \\
(b)	& A summary of poignant setbacks and solutions. \\
(c)	& A discussion of your time management. \\
(d)	& Anything else that you may wish to discuss.\\
\end{tabular}
\item \textbf{Future Developments.}
Describe speculative forwards-looking plans to build on the work.
\item \textbf{Appendix.}
Include Doxygen generated comments (copy-paste from Doxygen RTF output). You must include detailed class and function descriptions – but only include the documentation relevant to the classes that you have developed/extended. 
\end{enumerate}

\subsection{Assessment/Grading Criteria}
Marks above 40\% are available for the implementation of features that extend beyond the exercises completed in the practical sessions and will be awarded based upon the depth of subject knowledge demonstrated in the implementation and comments. Marks in the range 40-49\% will be awarded for a modest extension/synthesis of prior practical work. Marks in the range 50-59\% will be awarded for work that shows a clear understanding of the taught material by extending the lab exercises effectively. Marks in the range 60-69\% will be awarded for work that shows a good understanding of the taught material that demonstrates creativity in its application to the developmental work. Marks in excess of 70\% will be awarded for work that shows significant flair. The marks for the assignment will be allocated as follows:
\subsubsection*{Programmed Component (70 marks)}
\begin{enumerate}
\item \textbf{Program Features and Functionality} (35 marks)\\
\begin{tabular}{	l p{14cm} }
0-13 	& Required features have not been implemented and the work shows no development beyond practical exercises.\\
14-17   & The features and requirements have all been satisfied although their application could have been more effective.\\
18-20 	& Clear comprehension and synthesis of ideas completed in the practical sessions.\\
21-24	& Creative extension of practical work with innovative application of taught ideas.\\
25-35	& Exceptional demonstration of understanding.\\
\end{tabular}

\item \textbf{Code Structure and Clarity} (35 marks)\\
\begin{tabular}{l p{14cm} }
0-13 	& Absence of required code and/or coding errors resulting in serious compile and/or runtime errors.\\
14-17   & Code shows minimal decomposition into functions/classes and does not demonstrate an understanding of the conceptual elements of the module.\\
18-20  	& Code is neatly divided into functions and classes although elegant could be improved.\\
21-24	& Code demonstrates a good understanding of the conceptual module elements with an elegant decomposition.\\
25-35	& Exceptional application of the conceptual module elements with elegant class decomposition with appropriate class relationships.\\
\end{tabular}
\end{enumerate}

\subsubsection*{Written Component (30 marks)}
\begin{enumerate}
\item \textbf{Report} (30 marks)\\
\begin{tabular}{l p{14cm} }
0-11 	& The report or significant sections of the report are missing.\\
12-14   & The main sections of the report are included but adequate detail is lacking and presentation could be improved.\\
15-17 	& The report includes all sections although further detail could be included.\\
18-20	& All sections of the report are included, well-written and presented. The system documentation is comprehensive and appropriately written.\\
21-30	& The report is exceptional and addresses all required sections.\\
\end{tabular}
\end{enumerate}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Assignment Ideas}
Unique ideas beyond those listed here are highly encouraged.\\
\begin{tabular}{l p{17cm} }
$\bullet$	& Layered multi-track audio looper.\\
$\bullet$   & Matrix based sequencer/application (grid inspired (push/monome)).\\
$\bullet$	& Audio/MIDI based effect with strong object oriented structure (e.g. guitar pedal-board simulator).\\
$\bullet$	& Audio/MIDI sequencer.\\
$\bullet$	& An audio cue player for radio use with many files playable simultaneously with the touch of button.\\
$\bullet$	& A drum-loop slicer and manipulator.\\
$\bullet$	& Sound environment simulator (E.G. F-16 fighter jet cockpit, 007 James Bond car gadgets, nuclear powerstation during meltdown).\\
$\bullet$	& A generative/algorithmic music system (probabilistic drum patterns, MIDI modal-jazz harmoniser).\\
$\bullet$	& Non-linear music playback engine (e.g. Game level music supporting vertical \& horizontal progression parameters: time-spent-in-area, health, terrain, threat, time-of-day, weapon-choice, etc.)\\
$\bullet$	& A graphical audio file player/visualiser/oscilloscope.\\
\end{tabular}

\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
