\rule[.4cm]{\textwidth}{2pt}
\section{Practical 1 - Source Code Management}
\label{sec:prac1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this practical we will learn to use an Integrated Development Environment (IDE, either Xcode, Visual Studio or CLion) and the Source Code Management (SCM) tool Git: a very powerful development tool for managing and maintaining software.

On successful completion of this practical you will:

\begin{enumerate}
\item know how to set up and work with Git
\item how to use the console I/O methods in C++
\item have refreshed your C++ programming with a few simple exercises (and commits to the Git repository)
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Source Code Management - Git}
Source Code Management (SCM) historically involves lots of three letter acronyms: CVS (Concurrent Versioning System), SVN (Subversion), Git and lots of accompanying terminology. As we learn about SCM, each time a new term is introduced it will be in \textit{italics}. These terms will also be defined and described in the lecture part of the session.

SCM provides a sophisticated mechanism for safely and efficiently storing a complete history for your software development. It also provides a means of easily reverting to previous versions of your code and backing up files online. Anyone who knows anything about real software development uses source code management tools!

We will be using Git for everything in this module (including the assignment), so you will certainly become fluent in the associated terminology and tools. There is not enough time to discuss the ins and outs of Git right here and now, but after the session you should complete the reading set out in section \ref{sec:prac1:reading}.

\subsection{Getting Started}

Log into the Blackboard page for the module, browse to `Learning Materials' and run through the `Working at Home' section to make sure that you have all the software that you need and can set up projects using Git and build them using your IDE.  

You should now have a Git repository that exists online, the code is safely backed up and we can make a clone of the repository very easily on any machine with internet access and Git. It also makes it easy for multiple people to work on the code simultaneously pushing their changes to each other with minimal conflicts.

\subsection{C++ Program Structure}
Now that we have Source Code Management in place we will learn/review a bit of C++ to add to the repository. Return to your IDE and find the \texttt{main()} function. The code will look something like this:

\begin{center}
\includegraphics[width=0.6\textwidth]{images/Main}
\end{center}

Build and run the project. When all the files have \textit{compiled} and \textit{linked}, `\texttt{Hello,}' followed by your name, should appear in the console.

\subsubsection*{Familiar Code}
Form this starting point example we will learn how to handle console input and output in C++ (how to get text in and out of our programs in C++). Study the code identifying those parts of the program that you recognise and those that you do not, depending on which IDE you are using there will be small differences.

\subsubsection*{Comments}
\begin{center}
\texttt{// main.cpp}
\end{center}
As you may have learned previously, lines beginning with \texttt{//} indicate that the remainder of the line is a comment and is consequently ignored by the compiler. The C style comments, beginning \texttt{/*} and ending \texttt{*/}, are also valid and sometimes more appropriate.

\subsubsection*{Preprocessor Directive - Header File Inclusion}
\begin{center}
\texttt{\#include <iostream>}
\end{center}

Functionality can be made accessible to the program with the inclusion of header files. In this instance the standard header file \texttt{<iostream>}, short for \textbf{i}nput \textbf{o}utput s\textbf{tream}, provides access to the object \texttt{cout} (more on this shortly) which enables basic text input and output.

\subsubsection*{The \texttt{\textbf{main()}} Function}
\begin{center}
\texttt{int main()}
\end{center}
The entry point to the program is the \texttt{main()} function. When the program runs, the statements following the opening brace \texttt{\{} are executed in order. The program exits after the \texttt{return 0} statement has executed.

\subsection*{Program Structure - New Code - Console Output in C++}
The code that prints the text to the console output is:
\begin{center}
\texttt{std::cout << "Hello, World!\textbackslash{n}";}
\end{center}

\texttt{cout}, short for console out, is a \textit{stream} which refers to the standard output of the program: the console window. \texttt{cout} is a standard object declared in the file \texttt{<iostream>} within the \texttt{std} (\textbf{st}andar\textbf{d}) \textit{namespace}. To access the standard namespace \texttt{std} must precede the object name separated by a double colon \texttt{::} which is called the \textit{scope resolution operator}. For now, just remember that when you wish to display text on the console you must use the code \texttt{std::cout}, fuller discussions of objects and namespaces will come later.

In the above example, the string `Hello, World' is sent to the console window using the \textit{stream insertion operator} \texttt{<<}. Data to the right of the insertion operator is inserted to the stream on the left, in this case \texttt{cout}. The operator indicates the direction of information flow. It is possible to chain different types of data separated by insertion operators and send them to the console in a single statement. For example, modify the line of code to match the following:

\begin{center}
\texttt{std::cout << "Hello," << std::endl << "World!\textbackslash{n}";}
\end{center}

\texttt{endl}, short for \textbf{end} \textbf{l}ine, and is known as a stream \textit{manipulator} which ends the line of text and flushes the stream. \texttt{endl} is also located in the \texttt{std} namespace and, like \texttt{cout}, must also be preceded by the code \texttt{std::}

\subsubsection*{Printing Variable Values}
The `Hello, World' example demonstrates how a string `literal' can be printed to the screen. Values of differing types can be printed by separating them with insertion operators. For example, if an integer, float and char are all declared with the names \texttt{intNum}, \texttt{floatNum} and \texttt{charLet}, their values can be printed with the statement:

\texttt{std::cout << "integer value " << intNum << "{\textbackslash}nfloat value " << \\floatNum << "{\textbackslash}nchar letter " << charLet << std::endl;}

\subsection{Console Input in C++}
To retrieve input from the QWERTY keyboard, the object \texttt{cin} can be used, which, like \texttt{cout}, is also located in the standard namespace. \texttt{cin} may be used with the \textit{stream extraction operator} \texttt{>>} to store data typed at the keyboard into variables within our program. For example, to read a number into the integer variable \texttt{intNum}, the following code may be used:

\begin{center}
\texttt{std::cin >> intNum;}
\end{center}

This syntax also works for other variable types. To read a float, simply replace \texttt{intNum} with a variable of type float, e.g. \texttt{floatNum}. The conversion of the text to the appropriate data format is handled by the extraction operator. However, if the user enters invalid data, the \texttt{cin} object will switch to an error state where no further input can be extracted until the error is cleared.

\subsection{Exercise: Counter Print}
\label{sec:prac1:ex1}
Write a program that counts and displays on a new line all values up to the value of an integer number entered by the user.

When you have completed this program return to Sourcetree, click on `File status' where the file containing your \texttt{main()} function should show up as a modified file. When you are happy with the presentation of the code (indentation, comments, variable names, etc.), drag the file into the staged area and commit the code adding a commit message along the lines of `Practical 1 \ref{sec:prac1:ex1} Exercise, Counter Print completed'.

\subsection{Exercise: Counter Print Function}
\label{sec:prac1:ex2}
Now extend the program such that the counter loop is contained within a function that takes the entered value as an argument.

When you are happy with the program, return to Sourcetree and commit the changes adding a similar commit message for this exercise.

\subsection{Exercise: Factorial Function}
\label{sec:prac1:ex3}
Now write a completely new program that again reads in an integer from the user but this time calculates and displays the factorial of this number. Note that the factorial of a positive number ($n!$) is the product of all positive integers less than or equal to that number. For example $5!$ is calculated as follows:

$5 factorial = 5! = 5 \times 4 \times 3 \times 2 \times 1 = 120$

Create a function called \texttt{factorial} that checks to see that its input argument is positive and if so returns the factorial of this number.

Again make the code presentable and commit it with a suitable message.

\subsection{Exercise: Checking Out your Commits}
In Sourcetree make sure that all changes have been committed and pushed to the server. Click on `History', here you should see a plot showing the commit history of today's development. Right click on the commit for Exercise \ref{sec:prac1:ex1} and click `Checkout', click OK to confirm the change to the working copy. Return to your IDE, you should find the file containing the \texttt{main()} function updates to show your solution to exercise \ref{sec:prac1:ex1}.

Repeat this for Exercise \ref{sec:prac1:ex2} and finally for Exercise \ref{sec:prac1:ex3}. Now you can hopefully see how helpful source code management is, we have one copy of the code but many increments of the development that we can access at any time.

In practice commits tend to represent small increments in development in a single project rather than solutions to different exercises like this, but this is a good example of how to use commits.

When it comes to the assignment your commit history will reveal the pattern of development, whether you developed at a steady rate or whether you left it all to the last minute.

\subsection{Summary}
By using a Source Code Management tool like Git you can maintain a complete history of your development and roll backwards and forwards over the history at any time. Multiple branches of development can continue in parallel and be merged at a later date and many developers are able to work simultaneously on the same project sharing their commits as they go.

\subsection{Reading}
\label{sec:prac1:reading}
To go over the basic Git processes please read through these pages:\\ \url{https://docs.gitlab.com/ee/gitlab-basics/}\\
(the actual Git command line information is just for interest - Sourcetree manages these things for you but its always helpful to know what's going on!). \\Read through the sections:
\begin{itemize}
\item Command line basics
\item Start using Git on the command line
\item Create a project
\item Create a branch
\item Fork a project
\end{itemize}

Git is a very sophisticated management tool with a wide range of useful features, this webpage is a very useful Git resource: \url{http://git-scm.com/book}
