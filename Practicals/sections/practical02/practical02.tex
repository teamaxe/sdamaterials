\rule[.4cm]{\textwidth}{2pt}
\section{Practical 2: Classes}
\label{sec:prac02}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In last week's session we introduced Git and recapped basic input and output with the objects \texttt{cin} and \texttt{cout}. In this week's session we will learn about classes, and we will also learn how to organise our programs into separate files. By the end of this session you should have an understanding of:

\begin{enumerate}
\item the fundamentals of Object Orientated Programming.
\item the terms: Object, Class, Encapsulation, Interface, Constructor, Destructor.
\item how projects can be divided into multiple files to make lager programs easier to manage.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Classes and Object orientated Programming}
When C was extended into C++ by Bjarne Stroustrup in 1983, the introduction of the \texttt{class} was the most significant component that brought object orientated features to the language. Object orientated programming (OOP) is a method that naturally promotes an organised programming style that results in fewer bugs (double win).

We are object orientated beings that understand and interact with our surrounding environment through the perception and manipulation of objects. When observing a photograph, we quickly dissect the continuum of colours into an arrangement of objects, picking out people, landscapes, buildings, vehicles, and so fourth. Through prior experience, we recognise the attributes of, and expect particular behaviour from objects; even when confronted with a particular object for the first time. For example, telephones come in many different shapes and sizes, but with common attributes - colour, size, shape, number of buttons, etc. - and common behaviours - ringing, dialling, sending/receiving calls, etc. Objects can be understood as a set of attributes and behaviours.

\begin{center}
\resizebox{0.7\textwidth}{!}{\includegraphics{images/phones}}
\end{center}

Computer programs can be understood and designed in a similar fashion, where problems are solved with a program comprising an arrangement of interconnected objects, where each object has clearly defined attributes and behaviours. Before continuing to discuss the concepts of object orientated programming, it is important to appreciate the differences between an object and its \texttt{class}. A \texttt{class} declares a template for all objects of that type; an object is a single instance of a class with all of the specific attributes defined. Using the earlier telephone example, every physical example of a telephone is an \textit{object} (\texttt{myTelephone}, \texttt{yourTelephone}, etc.), however, at an abstract level, all telephones are identical and this abstract level is the \texttt{Telephone} \textit{class}.

%In last week's practical, several classes were introduced that perfectly illustrate this idea. Take for example the \texttt{TextButton} class, which can be used to create or \textit{instantiate} objects of type \texttt{TextButton}. A button can be added to the interface of an application by creating an instance or object of type \texttt{TextButton}, once the object exists its unique attributes can be set through the functions associated with the object. The text on the button can be set (\texttt{setButtonText("Click Me")}), the colour of the button can be set \texttt{setColour(TextButton::buttonColourId, Colours::peachpuff);} and the size of the button can be set (\texttt{setBounds(0, 0, getWidth(), getHeight())}).

%The arrangement of code in this format makes the use of the \texttt{TextButton} class quite easy to understand and use without having to know the inner workings of how a TextButton object actually works. Referring back to the telephone analogy it is not necessary to know how the telephone works in order to use it. The complications of converting the signal at the microphone to the digital signal that is transmitted is only of concern to the telephone developer, and thus hidden from the user. A developer may choose to completely change the inner workings of the telephone, but as long as the accepted telephone interface is adopted - numeric keypad, hand piece, etc. - the user (or \textit{client}) will be able to use the phone.

This conceptual model of objects and classes lends itself to the idea of abstraction: it is possible to use a telephone without understanding the complications of telecommunications, what's required is a familiarity with the \texttt{Telephone} \textit{interface} and it is possible to use any \texttt{Telephone} object. The complications of converting the signal at the microphone to the digital signal that is transmitted is only of concern to the telephone developer, and is hidden from the user. A developer may choose to completely change the inner workings of the telephone, but as long as its \textit{interface} is the same - numeric keypad, hand piece, etc. - the user (or \textit{client}) will be able to use the phone.

This form of abstraction relates to an important concept in object orientated programming: \textit{Encapsulation}, sometimes also referred to as `information hiding'. This is the process of hiding all of the implementation details of a class and only exposing what is necessary to configure and use an object of that class, the section of the class that is exposed is known as the \textit{public interface}.

To build programs in an object orientated language, we must learn how to design and build our own classes.

%\subsection{Writing a Class}
%Classes is similar to writing a structure in C with one important difference: in addition to data members (variables), a class declaration %includes a set of functions (behaviours) that act on the data members. Recall from your previous experiences the structure designed to %store the data associated with a MIDI note:
%
%\begin{verbatim}
%              struct MidiMessage
%              {
%                int number;
%                int velocity;
%                int channel;
%              };
%\end{verbatim}
%
%In this data centric view of information, the programmer might create functions associated with the data such as:
%
%\begin{verbatim}
%              void initialise (MidiMessage* note);
%\end{verbatim}
%
%which sets all members of the \texttt{note} structure to a default value.
%
%\begin{verbatim}
%              float getMidiNoteInHertz (MidiMessage* note);
%\end{verbatim}
%
%A function which returns the frequency in Hz of the note number in the structure.
%
%\begin{verbatim}
%              float getFloatVelocity (MidiMessage* note);
%\end{verbatim}
%
%A function that scales and returns the velocity into the range 0.0 - 1.0.

In object orientated programming enables us to divide programs up into logical units that contain both data and behaviours. In C++ we use the keyword \texttt{class} to declare our own types that contain variables (data members) along with functions (or behaviours) that act on these variables. The code below provides the definition for a simple \texttt{class} called \texttt{MidiMessage} which is designed to store, and manipulate, the information associated with a simple MIDI note.

\begin{lstlisting}
        class MidiMessage
        {
          public:
            //Constructor
            MidiMessage() = default;
  
            //Destructor
            ~MidiMessage() = default;
  
            //Mutator
            void setNoteNumber (int value) 
            {
              number = value;
            }
  
            //Accessor
            int getNoteNumber() const 
            {
              return number;
            }
  
            //Accessor
            float getMidiNoteInHertz() const 
            {
              return 440.f * std::powf (2.f, (number - 69.f) / 12.f);
            }

          private:
            int number = 60;
        };
\end{lstlisting}

Following this declaration, it is possible to create an object of type \texttt{MidiMessage} called \texttt{note} with the statement:
\begin{lstlisting}
        MidiMessage note;
\end{lstlisting}

Included within the code above is the syntax associated with declaring a class (\texttt{MidiMessage}), providing a public interface to that class and the concept of data encapsulation discussed above. 

\subsubsection{Exercise: Creating the Class \texttt{MidiMessage}}
Set up a new Git repository and empty C++ project called `Practical02', as described last week. 

Just above the \texttt{main()} function, declare the class \texttt{MidiMessage} as shown in the code fragment above. You will need to include the C++ library \texttt{cmath} to access the \texttt{std::powf()} function.

Test that the class operates as expected by writing code that executes the following in the \texttt{main()} function:
\begin{enumerate}
\item create an object (or instance) of the \texttt{MidiMessage} class.
\item print the default value of the note and its frequency.
\item read a \texttt{MidiMessage} number into a variable from the console and store it within the object.
\item print the new note number and frequency from the object.
\end{enumerate}

If you are unsure how to get started, look for clues in the next section where each part of the class is explained in detail. When you have completed the exercise commit and push your code.

\subsubsection{Class Declarations: \texttt{class}}
The \texttt{class} keyword indicates that the subsequent code will declare a class. The name given to the class is the text that immediately follows the keyword \texttt{class}. The content of the class is then contained within the following braces and the class declaration ends with a semicolon following the closing brace. Everything within these braces specifies the \textit{members} of the class, which includes \textit{data members} (the variables within the class), and the \textit{member functions} (the functions that act on the data members).

In this instance the class \texttt{MidiMessage} is declared as a unit that contains and operates on individual MIDI note messages. The guidelines for class name declarations is to use \textit{CamelCase}: all words should be joined with the first letter of each word in upper case.

Within the class the data members \texttt{int number;} and the member functions \texttt{setNoteNumber()}, \\\texttt{getNoteNumber()} and \texttt{getMidiNoteInHertz()} are all declared. The class contains everything that relates to the data.

\subsubsection{Access Specifiers: \texttt{public:} and \texttt{private:}}
The \texttt{public:} and \texttt{private:} keywords are referred to as \textit{access specifiers}. These labels provide regions within the class where data members (sometimes referred to as \textit{member variables}) and member functions are declared.

Beneath the \texttt{private:} access specifier is one of the data members that you would expect to find within a \texttt{MidiMessage} data structure: \texttt{int number;} to store the note number. Variables declared under \texttt{private} may only be accessed by the functions declared in the class. That is, the data member \texttt{number} may only be accessed by the member functions of the class: \texttt{MidiMessage()}, \texttt{$_{\widetilde{~}}$MidiMessage()}, \texttt{setNoteNumber()}, \texttt{getNoteNumber()} and \texttt{getMidiNoteInHertz()}.

Any functions or variables declared beneath the \texttt{public} access specifier can be accessed outside the class. This means that following the declaration:
\begin{lstlisting}
    MidiMessage note;
\end{lstlisting}

the note number can be set by the statement:
\begin{lstlisting}
    note.setNoteNumber (48);
\end{lstlisting}

the note number can be obtained by the statement:
\begin{lstlisting}
    note.getNoteNumber();
\end{lstlisting}

and the note frequency can be obtained by the statement:
\begin{lstlisting}
    note.getMidiNoteInHertz();
\end{lstlisting}

The following statement is \textbf{not} permitted because the member variable \texttt{number} is private:
\begin{lstlisting}
    note.number = 48;    //error
\end{lstlisting}

\textit{Encapsulation} is about hiding data members within the objects, protected by the member functions. This relationship is shown in the following diagram:

\begin{center}
\resizebox{0.5\textwidth}{!}{\includegraphics{images/access}}
\end{center}

Note that the public member space can be accessed externally through any object of type \texttt{MidiMessage}, but the private member space can only be accessed by the functions within the class. That is, the private member space is protected by the public member space.

You should make all data members (variables) and member functions private unless they need to be public. Data members are almost never declared public, if external access to data members is required \textit{accessor} functions that return their values, and \textit{mutator} functions that change their values should be created. Note that the accessor functions are declared \texttt{const} in the above example to indicate that they do not modify the internal (private) sections of the class. This is important and you should adopt this convention when writing your own classes and member functions.

Consequently, the only functions of interest to a programmer that needs to use the class is found in the \texttt{public} section, which is termed the \textit{public interface} to the class. This means that the internal mechanics of the class can be changed dramatically but the user's code will not be affected as long as the public interface remains the same.

\subsubsection{Object Initialisation: Constructors and Destructors}
Finally, the only sections of the class that are left to discuss are the constructor and destructor. A \textit{constructor} is a form of initialisation function, which is called automatically whenever a new object of the class is created. For example, the statement:
\begin{lstlisting}
    MidiMessage note;
\end{lstlisting}

will invoke a call to the constructor \texttt{MidiMessage()}, which in our example code is set to \texttt{default}, which means that the compiler generates a \textit{default} constructor for us. Note that \texttt{number} is assigned to an initial value of 60 (note C4), which means that objects can never be created with a garbage value as a note number. 

The constructor is an initialisation function and we could define our own constructor by replacing \texttt{= default;} with function braces \texttt{\{ \}} that enclose any code that we want to run whenever a \texttt{MidiMessage} object is created.

The destructor $_{\widetilde{~}}$MidiMessage() is a function which is called when \texttt{MidiMessage} objects go out of scope (ceases to exist). In this example code the compiler generates a default destructor, but if we would like to do something before an object goes out of scope then we can define our own destructor, as above. 

\subsubsection{Exercise: Viewing Construction and Destruction}
Define a constructor and destructor for the \texttt{MidiMessage} class that contain \texttt{std::cout} statements so that you can see when your object is created and destroyed via the console output. 

For example add the following to the constructor:
\begin{lstlisting}
    std::cout << "Constructor\n";
\end{lstlisting}
and to the destructor:
\begin{lstlisting}
    std::cout << "Destructor\n";
\end{lstlisting}

Now in \texttt{main()} create a few other objects of type \texttt{MidiMessage}, some within braces and some outside. Note when the console messages are called. The scope of objects and variables (i.e. when they are created and destroyed) is very important to understand! When you are ready commit and push your code.

\subsubsection{Exercise: Extending the Class \texttt{MidiMessage}}
As you may have already noticed, the \texttt{MidiMessage} class does not currently support velocity or channel storage. Extend the class such that it has velocity and channel data members that are initialised with appropriate values and with their own accessor and mutator functions. Now add a \texttt{getFloatVelocity()} function that converts the velocity and returns an amplitude (in the range 0.f - 1.f). Remember to add the \texttt{const} keyword to any accessor functions that do not modify the data members of the object (its internal state).

Now extend the main function to test all of your functions (by setting and printing the velocity and channel members) to ensure that the class works as expected. When complete, commit and push.

\subsection{Classes - Further Principles}
In this section, the \texttt{MidiMessage} class, will be developed such that the public member functions protect the data members from incorrect use. We will also add multiple constructors  to the class and correctly label accessor functions as \texttt{const}.

\subsubsection{Encapsulation}
As we have seen, encapsulation is the term ascribed to the object orientated principles of \textit{hiding complexity} and \textit{protecting data} within a class.

The complexity associated with an object is hidden within the member functions of the class and consequently is often not of concern to programmers that wish to use the class. In the \texttt{MidiMessage} class, the (limited!) complexity is in the conversion of the note number to the note frequency and the note velocity to note amplitude. By encapsulating this complexity within the class, the client may call the function \texttt{getMidiNoteInHertz()} oblivious to the calculation that is involved.

The data is protected from the programmer by only allowing access to the internal structure of an object through the accessor and modifier functions. In the \texttt{MidiMessage} class, the programmer is denied direct access to the private data members and must call the accessor/mutator functions instead \texttt{get/setNoteNumber()}, \texttt{get/setVelocity()} and \texttt{get/setChannel()}. A major benefit of this approach is that it becomes possible to prevent object data from being accidentally set to an incorrect or corrupted value. The object could be declared to protect itself from function calls such as \texttt{setNoteNumber(-20)}.

\paragraph{Exercise: MidiMessage with Data Protection}
Currently, the \texttt{MidiMessage} class does not protect against erroneous calls such as \texttt{setNoteNumber(-20)}. Modify the class such that range checks are performed on all of the mutator functions of the class, so that the member variables are only updated if the new value is valid. In the \texttt{main()} function, test these modifications within a basic program. Hints, use \texttt{if} statements to solve this one initially. Then improve on this by looking up and using the standard function \texttt{std::clamp()} in the \texttt{<algorithm> library}. When you are ready commit and push.

 \paragraph{Programming by Contract}
The previous exercise introduces a method of data validation which protects the programmer from setting invalid note data. However, with this added safety increases computation when the range of the input value is checked. Imagine a class called \texttt{MidiBuffer} containing an array of \texttt{MidiMessage} objects to be used within a musical melody. The designer of the class may want to implement a similar \texttt{setNoteNumber()} member function which subsequently calls the \texttt{setNoteNumber()} function on an object in the array. If the same range checking is performed in the \texttt{setNoteNumber()} member function of the \texttt{MidiBuffer} class there is redundant checking within the system. 

The Programming by Contract (or Design by Contract) design philosophy aims to minimise this redundancy by defining what constitutes valid and invalid input values to a given function. It is then the responsibility of the programmer to only provide valid input values. The class or function will behave appropriately provided that is is used appropriately. That is, each function defines a set of \textit{preconditions}, which must be met by the programmer, and \textit{postconditions} which will be met by the function. These pre- and postconditions form a contract between the the class and programmer which assigns the responsibility of data validation to the highest layer of data entry. 

\subsubsection{Constructors with Arguments (and Multiple Constructors)}
As we have seen, the constructor is called when an instance of the object is created. A constructor without arguments is called the \textbf{\textit{default constructor}}, and if we defined a default constructor (instead of using the \texttt{default} keyword) it would look something like this:

\begin{lstlisting}
    MidiMessage()
    {

    }
\end{lstlisting}

When an instance of this class is created:
\begin{lstlisting}
    MidiMessage note;
\end{lstlisting}

The data members will be initialised to the values they are assigned to when they were declared. However, we may want to initialise the object to a different set of values. To do this, a constructor with arguments can be declared in addition to the default constructor as follows:
\begin{lstlisting}
    MidiMessage (int note, int vel, int chan) 
     :  noteNumber (note), 
        velocity (vel), 
        channel (chan)
    {

    }
\end{lstlisting}

In this constructor, we use a \textit{member initialiser list} to initialise the data members to the values provided by the constructor's arguments. An instance of this class can then be created with a note number of 48, a velocity of 60 and a channel number of 5 as follows:
\begin{lstlisting}
    MidiMessage note (48, 60, 5);
\end{lstlisting}

It is possible for both of these (and other) constructors to coexist as they can be distinguished by their arguments. The first constructor has no arguments and the second has three - the compiler chooses the constructor matching the parameter values supplied. Declaring multiple functions with the same name but differing arguments is referred to as \textit{function overloading}.

\paragraph{Exercise: Adding a second constructor}
Add the constructor above to your \texttt{MidiMessage} class and check that it works. When ready commit and push.

\subsubsection{Accessor Functions and the \texttt{const} Keyword}
The keyword \texttt{const} should appear after all member functions that do not change the the internal state of an object, i.e. they access but do not change any of the object's data members. In the \texttt{MidiMessage} class, the function \texttt{getNoteNumber()} accesses but does not change the \texttt{noteNumber} member of the class. As such, the declaration for \texttt{getNoteNumber()} should be written:

\begin{lstlisting}
    int getNoteNumber() const
    {
        return number;
    }
\end{lstlisting}

The same applies for the accessor functions \texttt{ getVelocity()}, \texttt{getChannel()}, \texttt{getMidiNoteInHertz()} and \texttt{getAmplitude()}. This step is important as it enables your classes to be used effectively within other classes and programs.


\subsection{Managing Projects with Multiple Files}
As a C++ program grows, it becomes essential to divide the source code into separate files that are linked together at compile time. There are several upsides to arranging projects in this way:
\begin{enumerate}
\item the source code is easier to navigate and understand.
\item multiple programmers can work simultaneously on separate files of the project.
\item build times are reduced as only modified files need to be recompiled.
\end{enumerate}

\subsubsection{Units of File Division - Classes}
In C++, the class works as an ideal unit of division and it is good practice to place each class into a separate file; there are exceptions (i.e. nested or very closely related classes might be placed in the same file), but this is a good rule of thumb. 

\subsubsection{Linkage Mechanism - \texttt{\#include}}
The mechanism by which different files are connected together in a program is the \texttt{\#include} preprocessor. The preprocessor runs prior to the compiler and scans for code beginning with the the \texttt{\#} character. When the \texttt{\#include} command is encountered, the contents of the named file is inserted at that position in the file.

Often a file containing important declarations will be included in multiple files of the project which, unless precautions are taken, can cause problems associated with repeated declarations of the same function. Consider an arrangement of files where a music sequencer class is declared in the file \texttt{Sequencer.hpp} which includes the files \texttt{Drums.hpp} and \texttt{Melody.hpp}, which both include the file \texttt{MidiMessage.hpp}. This hierarchy is represented in the diagram below:

\begin{center}
\resizebox{0.7\textwidth}{!}{\includegraphics{images/include}}
\end{center}

As the files are connected, a compiler error would occur due to a repeated declaration of the class \texttt{MidiMessage} within the file \texttt{Sequencer.hpp}. To safeguard against this problem, preprocessor commands can be used to check whether a file has already been declared. The mechanism for achieving this involves the preprocessor directive \texttt{\#pragma once}. This flag ensures that the class declaration occurs only once. Consequently, the file \texttt{MidiMessage.hpp} should have the following structure:

\begin{lstlisting}
    #pragma once

    /** Class for storing and manipulating MIDI note messages */

    class MidiMessage
    {
        //Member declarations go here...

    };
\end{lstlisting}

When the project is built, the preprocessor will skip the declaration if it has been defined previously. These preprocessor statements are often referred to as \textit{guards} as they guard against multiple includes; you must remember to use them in all header files to avoid errors, which may not emerge until later on in in your development.

\subsubsection{Separating Interface from Implementation}
A further division that helps in the organisation of code is the separation of interface from implementation. That is, all declarations should appear in a \texttt{.hpp} file and all implementation should be placed in a \texttt{.cpp} file. Take the following cut-down \texttt{MidiMessage} example class:
\newpage
\begin{lstlisting}
    class MidiMessage
    {
    public:
        //Constructor
        MidiMessage() = default;
  
        //Destructor
        ~MidiMessage() = default;
  
        //Mutator
        void setNoteNumber (int value)
        {
            number = value;
        }
  
        //Accessor
        int getNoteNumber() const
        {
            return number;
        }
  
        //Accessor
        float getMidiNoteInHertz() const
        {
            return 440.f * std::powf (2.f, (number - 69.f) / 12.f);
        }
        
    private:
        int number = 60;
  };
\end{lstlisting}

This can be separated into two files such that the interface (declaration) of the class is in a file called \texttt{MidiMessage.hpp} and the implementation is in a file called \texttt{MidiMessage.cpp}. The code for \texttt{MidiMessage.hpp} is shown below:

\begin{lstlisting}
  #pragma once
  /** Class for storing and manipulating MIDI note messages */
  class MidiMessage
  {
  public:
      /** Constructor */
      MidiMessage() = default;

      /** Destructor */
      ~MidiMessage() = default;

      /** Sets the MIDI note number of the message */
      void setNoteNumber (int newNoteNumber);

      /** Returns the note number of the message */
      int getNoteNumber() const;

      /** Returns the MIDI note as a frequency */
      float getMidiNoteInHertz() const;

  private:
      int noteNumber;
  };
\end{lstlisting}
\label{sec:prac03:header}

Notice that the interface .hpp (\textit{header}) file now contains only the information that is of interest to a programmer using of the class: it contains all of the function declarations of the class without having to wade through the implementation. To use the class, programmers simply \texttt{\#include} the header file at the top of the file in which it is used.

\texttt{MidiMessage.hpp} is included at the top of \texttt{MidiMessage.cpp} which is shown below:

\begin{lstlisting}
#include "MidiMessage.hpp"

void MidiMessage::setNoteNumber(int newNoteNumber)
{
    noteNumber = newNoteNumber;
}

int MidiMessage::getNoteNumber() const
{
    return noteNumber;
}

float MidiMessage::getMidiNoteInHertz() const
{
    return 440.f * std::powf (2.f, (noteNumber - 69.f) / 12.f);
}
\end{lstlisting}
\label{sec:prac03:implementaion}

The implementation file contains all of the function definitions. Before each function name is the name of the class to which it belongs followed by the \textit{binary scope resolution operator} \texttt{::} which binds the function to the now separate class declared in the header file.

With both of these files added to the project tree the \texttt{MidiMessage} class can be used simply by including the appropriate header file:

\begin{lstlisting}
  #include "MidiMessage.hpp"
\end{lstlisting}

and an object of type \texttt{MidiMessage} can be created with the statement:

\begin{lstlisting}
  MidiMessage note;
\end{lstlisting}

\subsubsection{Exercise: Separating the MidiMessage Class Into Separate Files}
In your project you should have a working program with the \texttt{MidiMessage} class declared at the top. Please see the videos provided along with this week's practical files on Blackboard to see how to separate a class into separate files. When you are complete, commit and push.

Now the processes involved in creating your own library should be clearer!

\subsection{Commenting Header Files}
As programmers using a class are usually most interested in the class interface, the \texttt{.hpp} file is usually the first stop to find out how to use the class. Consequently, the header file is also a good place to write comments about the class and its members. The code block for the header file on page \pageref{sec:prac03:header} shows that each function is documented with a short description. Notice that comments are positioned above the function they describe and start with a \texttt{/**} and end in a \texttt{*/}. This comment is like an ordinary comment but the double asterisk \texttt{**} indicates that this is a \textit{doxygen} comment block. We will learn later how to produce code documentation automatically with doxygen in later weeks, but for now, just use this comment format above each function in the header files of your classes.

\subsubsection{Exercise: Fully Commented MidiMessage Class}
Expand the class to include accessors and modifiers for the channel and velocity parameters. Ensure that you encapsulate the data correctly by ensuring that the modifiers cannot contain incorrect values. Also, ensure that all accessors are marked with the \texttt{const} keyword and ensure that each new function contains its own doxygen style comments. When ready, commit and push.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Additional/ Homework Exercise - A MidiMessage Class}
Think about how you might expand the class such that it is capable of storing any type of MIDI message. Think about the behaviour and attributes of this class, and make a list of the data members and functions that it might include. Implement this class and test that it works within the enclosed environment of basic C++ project and then commit and push. 

\paragraph{Reading}
Complete all exercises and read the following:

\begin{enumerate}
    \item Search for the ebook `Professional C++' (4th edition) in the UWE library \url{https://www1.uwe.ac.uk/library/} and read:
    \begin{enumerate}
        \item Chapter 5, pp. 124-129
        \item Chapter 7, pp. 199-218
    \end{enumerate}
\end{enumerate}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
