\rule[.4cm]{\textwidth}{2pt}
\section{Practical 3 - Dynamic Memory Allocation and Linked Lists}
\label{sec:prac03}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this practical we will learn about dynamic memory management - allowing our programs to allocate memory at runtime (rather than only at compile time). On successful completion of this practical you will have a greater understanding of:

\begin{enumerate}
\item program memory structures including the stack and heap.
\item dynamic memory allocation using \texttt{new} and \texttt{delete}.
\item how to use dynamic memory allocation to build dynamic containers and data structures like vectors and linked lists.
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Memory Structures and Management}
When a C/C++ program runs, it can make use of three separate memory categories.

\begin{enumerate}
\item \textbf{Code} Where the program code resides, consisting of all the machine instructions for the program. This is where all of the functions (including member functions) reside in memory.
\item \textbf{Stack} A relatively small area of memory allocated to each thread of execution for fast access to variables and objects.
\item \textbf{Heap} A large area of memory which may be allocated to a program dynamically at runtime.
\end{enumerate}

Previously in your C and C++ programming work, we have only used only the first two memory categories. The functions that you have written occupy the code memory and the variables created within these functions occupy the stack. That is, whenever a variable or object is created, memory on the stack is used, for example:
\begin{verbatim}
   int counter;
   float frequency;
   char name[256];
   MidiMessage note;
\end{verbatim}

When a variable on the stack goes out of scope, the variable no longer exist and cannot be used. This arrangement results in computationally efficient memory access, however the size of the memory is fixed at compile time (i.e. the size of arrays are set by the programmer).

\subsubsection{Dynamic Memory Management and the Heap}
In many situations the quantity of memory required for a piece of data cannot be known at compile time; in these circumstances the stack cannot be used. For example, consider a simple audio file player that allows the user to select an audio file from the file system which is loaded into an array and streamed to the audio output (we will be looking at programs like this in the near future). The program will need to load files of differing sizes - how big should the array be?

One method is to declare an array on the stack that is larger than any file that the user might wish to use, which is impractical and wasteful. The other method is to establish how big the file is at runtime and use dynamic memory allocation to allocate exactly the right amount of memory.

Memory that is allocated dynamically at runtime belongs to the heap.

\subsubsection*{Dynamic Memory Allocation in C++ with \texttt{new}}
In C++ the keyword for allocating memory on the heap is \texttt{new}. For example, the following statement allocates enough memory for a single floating point number:

\begin{verbatim}
   new float;
\end{verbatim}

The \texttt{new} keyword allocates the requested memory on the heap and returns the address of this memory, which should be assigned to a pointer, for example:

\begin{verbatim}
   float *floatPoint = nullptr;    //declare pointer
   floatPoint = new float;    	    //pointer now points to float on the heap
\end{verbatim}

The pointer \texttt{floatPoint} now points to a float allocated on the heap, which can be accessed using the normal pointer notation. Note that when the pointer is declared it is set to point to \texttt{nullptr} to ensure that it can never point to a \textit{garbage} value, which can be dangerous. If a pointer containing a garbage value is dereferenced, it can cause unpredictable problems including a complete program crash. To prevent this, it is good practice to set your pointer to point to \texttt{nullptr} so that its value can be checked to see if it points to memory or not.

\begin{verbatim}
   if (floatPoint != nullptr)      //test pointer for null
      *floatPoint = newValue;      //if true it's safe to dereference
\end{verbatim}

Arrays of data can also be allocated on the heap for example the following fragment:
\begin{verbatim}
   float *floatPoint = nullptr;	   	//declare pointer
   floatPoint = new float[10];  	   //pointer now points to float on the heap
\end{verbatim}
\texttt{floatPoint} now points to the first element of a 10 element float array.

\subsubsection*{Dynamic Memory Deallocation in C++ with \texttt{delete}}
Unlike variables and objects that are allocated on the stack, data stored on the heap must be explicitly deallocated using the \texttt{delete} keyword. For example if \texttt{floatPoint} was assigned as follows:
\begin{verbatim}
   float *floatPoint = nullptr;    //declare pointer make it point to null (important)
   floatPoint = new float;         //pointer now points to float on the heap
\end{verbatim}

it points to a memory location on the heap. When this memory is no longer needed it should be deleted with the statement:

\begin{verbatim}
   delete floatPoint;       //release the memory
\end{verbatim}

Note however that an array allocated with:
\begin{verbatim}
   float *floatPoint = nullptr;	    //declare pointer
   floatPoint = new float[10];      //pointer now points to float on the heap
\end{verbatim}

should be deallocated with the \texttt{delete[]} keyword:
\begin{verbatim}
   delete[] floatPoint;     //release the memory
\end{verbatim}

The structure of a class often makes the allocation and deallocation of memory quite straightforward and safe: memory allocation takes place in the constructor, when an object is created and memory deallocation often takes place in the destructor, called when an object expires. Memory may be allocated and deallocated elsewhere, but all allocations made with \texttt{new} \textbf{must} be deallocated with a subsequent call to \texttt{delete}. Allocation without deallocation results in what's called a \textit{memory leak}, and this a very prevalent cause of problems and bugs in C/C++ programs.

\subsubsection{Practice Exercise -  Using Dynamic Memory Allocation for the First Time}
Take a fork of the`Practical03-CommandLineTool' repository found at  \url{https://gitlab.uwe.ac.uk/sda}, as described in previous weeks. In your fork rename the repository `Practical03-ArraysLinkedLists'.

This exercise is just to practice the syntax and has no purpose beyond that.
\begin{enumerate}
\item Practice creating a single \texttt{float} variable dynamically and testing that it works by writing a value to it and printing it out again before finally deleting the memory allocated to this float.
\item Practice creating a single array of \texttt{float}s dynamically, accessing and printing each member before deleting the array.
\end{enumerate}

\subsection{Containers - Dynamic Arrays and Lists}
As dynamic memory allocation allows us to allocate memory at runtime, it makes it possible to create sophisticated array type structures that can grow and shrink dynamically (for reference, these types of memory structure are often referred to as \textit{containers}).

We will create two types of container throughout the remainder of this practical session: a \texttt{Vector} (sometimes called a dynamic \texttt{Array}) and a \texttt{LinkedList}.

\subsection{Dynamic Arrays (Vectors)}
Dynamic Arrays are containers that maintain a dynamically allocated array of variables that can grow and shrink as new items are added to, and removed from, the array. When the vector is created it is empty and has no memory allocated. When an item is added, an array is allocated that is large enough to store one element. Every time a new item is added a new array is reallocated that is one element larger than the old array, then the old array contents are copied across and the new item is copied to the end. Finally the old array is deleted and replaced by the new array.

\subsubsection{Exercise - Creating A Dynamic Array}
Create a new class called \texttt{Array} which manages a dynamic array of \texttt{float}s. The class should be created in separate \texttt{.h/.cpp} files as described last week and documented using the doxygen \texttt{/** comments */}. It should include only two private data members, the number of items in the array and a pointer to a dynamically allocated memory block that stores the data.

The class should include the following functions:\\
\begin{tabular}{ l l }
  \texttt{Array();} & a constructor that initialises the data members\\
  \texttt{$\sim$Array();} & a destructor that deletes the memory allocated to the array\\
  \texttt{void add (float itemValue);} & which adds new items to the end of the array\\
  \texttt{float get (int index);} & which returns the item at the index\\
  \texttt{int size();} & which returns the number of items currently in the array\\
\end{tabular}

To test that your array works, write a series of tests in a function called \texttt{bool testArray()} that is called once in \texttt{main} to check the class works. In this function, an \texttt{Array} should be created where all of the above functions are tested to see if they operate and return the right values, if everything tests out, it should return true, if something goes wrong it should return false. For example, check that the array has a size of 0 when empty, and that the array size enlarges as items are added, and that these items are recalled correctly.

An example \texttt{testArray()} and \texttt{main()} function is provided in this week's practical folder on \texttt{StudentShared}.

\subsection{Linked Lists}
As you will have noticed from the exercise above, the dynamic array is quite inefficient when it comes to adding new items, as it has to create an entirely new array and copy the contents of the old array across. However, once it is allocated it is very fast to access each element. An alternative container is a \texttt{linked list} which has a completely different structure which makes it fast to add and remove elements, but slower to individually access each element, a tradeoff that programmers need to be aware of when choosing a particular container type.

A linked list is a container that manages a chain of nodes, where each node contains some data and a reference (pointer) to the next node.

\begin{center}
\resizebox{0.2\textwidth}{!}{\includegraphics{images/Node}}
\end{center}

A linked list for storing floating point numbers would comprise a chain of nodes that contain a single \texttt{float} and a single pointer that points to the next node. This is shown in the diagram below, which shows a linked list of three elements.

\begin{center}
\resizebox{0.8\textwidth}{!}{\includegraphics{images/LinkedList}}
\end{center}

The linked list is accessed via a pointer to the first node, which is called the \textit{head}. Each subsequent node is accessed via the link-pointer which always points to the next node in the chain and the last node's link-pointer is set to \texttt{nullptr} to mark the end of the list.

When the linked list is created, it has no memory allocated for the storage of floats, but when an item is added a single node is created dynamically and linked on to the end of the list. This is achieved by finding the \texttt{Node} at the end of the list and setting it's pointer to point to the new \texttt{Node}, which is itself set to point to \texttt{nullptr} to mark the new end of the list.

\subsection{Exercise - Creating a Linked List}
Create a new class called \texttt{LinkedList} which manages a linked list of \texttt{float}s. The class should be created in separate \texttt{.h/.cpp} files as above and again commented in a doxygen style. The class should have a private data structure declaration for each linked list element, which is as follows:

\begin{verbatim}
    struct Node
    {
        float value;
        Node* next;
    };
\end{verbatim}

And the \texttt{head} should also be declared privately as a pointer to a \texttt{Node}:

\begin{verbatim}
		Node* head;
\end{verbatim}

The class should include the following functions: \\
\begin{tabular}{ l l }
  \texttt{LinkedList();} & a constructor that initialises the \texttt{head} to point to \texttt{nullptr} \\
  \texttt{$\sim$LinkedList();} & a destructor that goes through and deletes all nodes in the list \\
  \texttt{void add (float itemValue);} & which adds new items to the end of the array \\
  \texttt{float get (int index);} & which returns the value stored in the \texttt{Node} at the specified index \\
  \texttt{int size();} & which returns the number of items currently in the linked list \\
\end{tabular}

To test that your linked list works by writing a similar test function to the one that you used for the \texttt{Array} class by creating a function called \texttt{bool testLinkedList()} that is again called in \texttt{main()} to check the class works.

\paragraph{Hints}
\begin{itemize}
\item The \texttt{size()} function should work by traversing the list from head to tail counting each link until it arrives at the \texttt{nullptr}.
\item The \texttt{get()} function should work by traversing the list from the head counting each link until the counter equals the \texttt{index} and then returns the value stored at that link.
\item The \texttt{add()} function should work by allocating a new node with the value of the argument and a link-pointer assigned to \texttt{nullptr}, it should then traverse to the end of the list and make the last item's link-pointer point to the new node.
\end{itemize}

\subsection{Extensions}
If you have completed everything above, that is most impressive! However, there are many features that you should add to your containers to make them more advanced. Completing these extensions will dramatically improve your algorithmic programming technique. For interest, I have been asked to write a function that would reverse a linked list on a number of occasions in job interviews.

Add the following functions to your container classes:\\
\begin{tabular}{ l l }
  \texttt{void remove (in index);} & which removes the specified item from the list \\
  \texttt{void reverse();} & which reverses the order of all the items in the list \\
\end{tabular}

\subsection{Reading}
\begin{itemize}
\item Read up on singly linked lists by reading ``Algorithms Third Edition in C++''. Chapter 3.3 Linked Lists, which is available via UWE's Safari Books Online subscription:\\ \url{http://proquestcombo.safaribooksonline.com.ezproxy.uwe.ac.uk}. This book covers lots of great and more advanced C++ ideas that you may wish to read about.
\item This video also provides a good overview of how linked lists work.\\ \url{https://www.youtube.com/watch?v=LOHBGyK3Hbs}
\item There's also lots and lots of related resources online - see if you can find a resource that speaks your language.
\end{itemize}
