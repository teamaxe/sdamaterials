\rule[.4cm]{\textwidth}{2pt}
\section{Practical 4 - Templates and Overloading}
\label{sec:prac04}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%

In this practical we will look at two important features of the C++ programming language called \textit{templates} and \textit{overloading}. Templates allow functions and classes to be written that can operate on \textit{generic types}, while operator overloading enables programmers to define how unary and binary operators behave when acting on their own types (classes). We will build on the dynamic array that we created in last week's practical so hopefully you have that finished. By the end of this practical you should have an understanding of the following:

\begin{enumerate}
\item how to overload functions.
\item how to declare and use template functions.
\item how to declare and use template classes.
\item how to overload operators within user-defined classes.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Getting Started}

\begin{enumerate}
\item Log into the Macintosh.

\item If you have not already create a folder for this module in your \texttt{Desktop} or \texttt{Documents} folder. 

\item Now navigate to the network folder \texttt{/Desktop/StudentShared/fet/CSCT} and copy the folder \texttt{\textbf{/SoftwareDevelopmentForAudio-UFCF94-15-3/Practical04}} to your folder for this module. Inside you will find a dynamic array class that you can use to check your own solution to last week's practical exercises. 

\end{enumerate}

\begin{center}
\fbox{\parbox{0.9\textwidth}{\textbf{If you encounter any problems with any of the above steps do your best to resolve them.  Ask the lecturer if you have a question or a problem.}}}
\end{center}

\subsection{Templates}
Templates are one of C++'s most powerful features as they enable programmers to create a \textbf{set} of overloaded functions or a \textbf{set} of related classes by writing the code as a single generic function or class. A technique which is referred to as \textit{generic programming}. Read on, and then be sure to read this back later to ensure that you have understood what this means.

\subsubsection*{Another Level of Abstraction}
Consider, the function below which returns the maximum of its two arguments:

\begin{verbatim}
int max (int a, int b)
{
    if (a < b)
        return b;
    else
        return a;
}
\end{verbatim}

In the \texttt{max()} function, it does not matter what the values of \texttt{a} and \texttt{b} are, it always returns the maximum of the two. Now consider what a \texttt{max()} function for a \texttt{float} type would look like: it would be identical, only the `\texttt{int}s' would be replaced with \texttt{float}s. Templates make it possible to write a single \texttt{max()} function that is specified as a generic function or class capable of operating on any type (\texttt{float}s or \texttt{int}s, \texttt{chars} or any other type). 

\subsubsection{Overloading Functions}
Before getting straight into templates, lets look at the related area of function overloading, which will act as a helpful stepping stone on the way. C++ allows functions like \texttt{max()} to be \textit{overloaded} for other types. For example, if you wanted to find the maximum of two doubles you could also declare the function:
\begin{verbatim}
double max (double a, double b)
{
    if (a < b)
        return b;
    else
        return a;
}
\end{verbatim}

\subsubsection*{Exercise - Overloading Functions}
Make sure that everything that you completed last week is committed and and in your GitLab account create a new project called `Practical4-TemplatesOverloading' clone this project and then copy the final Xcode project that \textbf{you} created last week called `P3 - Arrays and Linked Lists'. Remember to include the source files and the `.gitignore' file and then when you are happy that you have done this correctly, commit the updates to the prject - we are ready to begin. 

In \texttt{main.cpp} define and declare both the integer and floating-point versions of the \texttt{max()} function and in \texttt{main()} write out a few test functions to make sure that they work correctly. Add to the \texttt{double} version of the function the line \begin{verbatim}std::cout << "max for doubles\n"\end{verbatim} and a similar line for the \texttt{int} version so that you can see when each function is called when you run the tests. 

\subsubsection{Template Functions}
Notice that the body of these two functions are identical (with the exception of the \texttt{std::cout} section you added), the only real differences are the types of the arguments and the return type. Template functions provide a way of abstracting away these differing argument and return types. So it is possible to make a single template function for \texttt{max()} that would work for \texttt{int}s, \texttt{float}s, \texttt{char}s, and so fourth. This means that the types of the function must be expressed as parameters in a similar way to which the values passed into the function are expressed as the parameters \texttt{a} and \texttt{b}. Function templates are preceded with what is termed the \textit{template prefix}: \texttt{template <typename Type>} where the keyword \texttt{Type} may then be used within the function whenever the type would ordinarily appear. For example a single template function for \texttt{max()} can be written as follows:

\begin{verbatim}
template <typename Type>
Type max (Type a, Type b)
{
    if (a < b)
        return b;
    else
        return a;
}
\end{verbatim}

\subsubsection*{Exercise - Template Functions}
Write template functions and tests for the following:\\
\begin{tabular}{ p{3cm} p{13cm}}
  \texttt{min();} & a function that returns the minimum of its two arguments.\\
  \texttt{add();} & a function that returns the sum if its two arguments.\\
  \texttt{print();} & a function that takes two arguments an array of a generic type and an \texttt{int} specifying the size of the array. The function should then print out every item in the array to the console.\\
  \texttt{getAverage();} & a function that takes two arguments: an array of a generic type and an \texttt{int} specifying the size of the array. The function then calculates the mean average and returns this value as a \texttt{double}.\\	
\end{tabular}

\subsubsection{Template Classes}
This idea of writing generic code that is capable of dealing with data of many different types can also be extended to classes. That is, a class can be written once where it is possible to create objects that work with \texttt{int}s, \texttt{float}s, \texttt{char}s, and so fourth. Consider the dynamic array example developed last week that manages an array that is capable of storing floating-point numbers. If you wanted to create a dynamic array for storing integers, you could do a copy and paste job and call the new class \texttt{ArrayInt}, and then whenever you make a change or add a new function, you would have to make sure that you remember to copy it across to the old \texttt{ArrayFloat} class. However, trying to maintain two bits of similar code is a classic cause of problems and templates allow you to write a single class that is able to store arrays of different types. This is yet again, another example of the `single source of truth' concept. 

Let's look at a pointless but simple example so that we can see how to work the syntax of a basic template class. This class simply contains a variable that can be of any type with accessor and mutator. Now notice that the class declaration and implementation are all in the header file, this is a requirement of template classes \textbf{so you will not have an accompanying \texttt{.cpp} file}.
\newpage
\begin{verbatim}
template <class Type>
class Number
{
public:
    Type get() const
    {
        std::cout << "yes value";
        return value;
    }
    
    void set(Type newValue)
    {
        value = newValue;
    }
    
private:
    Type value;
};
\end{verbatim}

To then create an object of type \texttt{Number} the type required must also be specified, for example:
\begin{verbatim}
    Number<float> numf;
    Number<int> numi;
    Number<std::string> numstr;
\end{verbatim}

values can then be set:
\begin{verbatim}
    numf.set (0.5f);
    numi.set (2);
    numstr.set ("yess");
 \end{verbatim}
 
 and values can be printed:
 \begin{verbatim}
    std::cout << numf.get() << "\n";
    std::cout << numi.get() << "\n";
    std::cout << numstr.get() << "\n";
 \end{verbatim}
  
 \subsubsection*{Exercise - Template Class \texttt{Number}}
 Create a header file for the \texttt{Number} class and give it a trial run. Creating objects, adding values and printing them out again. 
 
\subsubsection*{Exercise - Template Class \texttt{Array}}
Find your \texttt{Array} class and adapt it so that it can store arrays of any type and write some test procedures to ensure that all is working. Note that you will have to move all the implementation to the header file and delete the cpp file. Don't worry the files will still be saved in your commit history. 
 
 \subsubsection{Operator Overloading}
Operator overloading is another valuable feature of C++ that enables standard operators to be overloaded as they are often more readable and intuitive than using functions. This is easily understood by considering the code:
\begin{verbatim}
      y = times (x, 5);
\end{verbatim}

compared with:
\begin{verbatim}
      y = x * 5;
\end{verbatim}

In the second statement the code is much quicker to read and nicer to look at. This is an example of \textit{Syntactic Sugar} - features of programming languages which are designed specifically to make the syntax easier to read.

When performing operations on objects the standard symbols can be overridden to give intuitive meanings. For example recall from last year the use of character arrays for the storage of strings:
\begin{verbatim}
      char string1[10] = {"hello"};
      char string2[10] = {"hello"};
\end{verbatim}

if we wanted to test to see whether the contents of the two strings was identical it might be tempting to construct a statement as follows:
\begin{verbatim}
      if(string1 == string2) //compares the addresses
      {
\end{verbatim}

however this would not work as the name of an array without square brackets is actually the address of the first array element - which would never be the same in this situation. To compare two strings like this we have to use the function for string comparison as follows:

\begin{verbatim}
      if (strcmp (string1, string2) == 0) //compares the strings
      {
\end{verbatim}

Which is not so readable. Fortunately in C++ the \texttt{==} operator can and has been overridden for the \texttt{std::string} class so that strings can be compared in this way. Thus, the equivalent code using the C++ std::string class would be as follows:
\begin{verbatim}
      std::string string1 ("Hello");
      std::string string2 ("Hello");
      if (string1 == string2)	//compares the strings :)
      {
\end{verbatim}
\newpage
Many operators can be overloaded to enable a variety of different features. We will consider only very of basic examples here. For example imagine the \texttt{MidiNote} class that we developed a few weeks back. We want to establish whether two MIDI messages are exactly the same which currently involves the process shown:
\begin{verbatim}
   if (note1.getNote() == note2.getNote())
   {
      if (note1.getChannel() == note2.getChannel())
      {
         if (note1.getVelocity() == note2.getVelocity()) 
         {
            //do something
         }
      }
   }
\end{verbatim}

Using operator overloading we can compare the notes simply by
\begin{verbatim}
   if (note1 == note2)
   {
      //do something
   }
\end{verbatim}

To achieve this the \texttt{==} operator is overloaded in the \texttt{MidiNote} class as follows:
\begin{verbatim}
   bool operator==(const MidiNote& otherNote)
   {
      if (getNote() == otherNote.getNote())
      {
         if (getChannel() == otherNote.getChannel())
         {
            if (getVelocity() == otherNote.getVelocity()) 
            {
               return true;
            }
         }
      }
      return false;
   }
\end{verbatim}

When the comparison:
\begin{verbatim}
   if (note1 == note2)
   {
      //do something
   }
\end{verbatim}

takes place this operation is performed within the \texttt{note1} object where the \texttt{note2} object is the \texttt{otherNote}. Notice that a \textit{reference} is used as the argument, this is more efficient than passing by value because it passes the argument by reference. The use of the \texttt{const} keyword also ensures that the \texttt{otherNote} cannot be changed.

\subsubsection{Exercise: Operator Overloading}
Refer back to your \texttt{Array} class and add a method to test whether two array objects are the same using the \texttt{==} operator, i.e. that they are the same size and the contents of each element is also the same. Also overload the \texttt{!=} operator to enable tests for different arrays.

Now the \texttt{Array::get()} function is less than ideal as it would be nice if we could use the array syntax \texttt{[]} to refer to the elements of our array, so that for example \texttt{array[0]} could be used. This addition could also allow us to modify the array elements after they have been added. Implement this operator in the \texttt{Array} class, you may need to do some online reading or refer to the suggested text in the \texttt{Homework} section.

\subsubsection{Exercise: LinkedList Template}
Apply the extensions that you have made to the \texttt{Array} class to the \texttt{LinkedList} class.

\subsection{Homework}
Read the chapters on templates (Chapter 14. Templates) and operator overloading (Chapter 11. Operator Overloading) in the textbook - `C++ How to Program, Sixth Edition' By: Deitel and Deitel, which is available as an ebook through the library at the following link:\\
\href{https://www.dawsonera.com/abstract/9781447930150}{https://www.dawsonera.com/abstract/9781447930150}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


