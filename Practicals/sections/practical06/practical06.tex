\rule[.4cm]{\textwidth}{2pt}
\section{Practical 6 - Audio and MIDI In Juce}
\label{sec:prac06}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this practical we will learn about basic MIDI and Audio input/output (I/O) processes in application programming and how these processes can be set up and managed using Juce. By the end of this session you should be familiar with:

\begin{enumerate}
\item Audio and MIDI support in Juce applications.
\item Audio/MIDI callback routines and buffering methods used in audio applications.
\item A cautionary introduction to threading.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Audio and MIDI Functionality in Audio Applications}

Computer applications usually remain idle until some event is performed by the user. In response to this event an action is invoked. In prior exercises we have seen how this process is facilitated using many of the Juce GUI components last week (\texttt{TextButtons}, \texttt{Sliders}, etc.); the GUI is clicked or modified in some way and a callback function is called where we can perform corresponding actions.

This mechanism also applies to the handling of MIDI and audio within applications: incoming MIDI and audio I/O is managed with the use of callback functions. In this practical we will learn how to add audio and MIDI functionality to our Juce applications. 

\subsection{Audio and MIDI Functionality in Juce}
The process of adding audio and MIDI features is simplified with the use of inheritance in Juce. By inheriting from the relevant MIDI and audio classes we can respond to and send MIDI events, and we can also process audio in our application. 

As was stated earlier, the process for setting up audio/MIDI callbacks is comparable to setting up a button callback, which was as follows:
\begin{enumerate}
\item A \texttt{Button} must be created to which we would like to respond
\item A \texttt{Button::Listener} object implementing a \texttt{buttonClicked()} callback function is added as a listener to the button
\end{enumerate}
Following this process, when a button is clicked, the click is broadcast to its listeners and the callback function is invoked. 

This process is analogous to setting up the audio/MIDI callbacks:
\begin{enumerate}
\item An audio device manager must be created through which we would like to receive and send audio/MIDI
\item An object containing a MIDI/Audio callback function is registered with the audio device manager
\end{enumerate}

Central to all audio/MIDI activity in Juce is the \texttt{AudioDeviceManager} which as its name suggests encapsulates all of the audio settings and manages the connection to all audio/MIDI devices. Consequently, applications that need to receive or send audio and/or MIDI, should contain and manipulate an object of this class. Let us examine this process initially for MIDI. 

\subsection{MIDI Functionality in Juce}
\subsubsection{Exercise - Adding MIDI Functionality in Juce}
Take a fork of the \texttt{Practical06a-JuceBasicWindow} repository found at  \url{https://gitlab.uwe.ac.uk/sda}, as described last week. Rename your fork `Practical06a-Midi' then clone this repository with the rest of your SDA projects (remember it must be within the same folder as your Juce repository (but not in the Juce repository)). Build the project and ensure that it works.

Follow these steps:
\begin{enumerate}
\item First, we need to add an \texttt{AudioDeviceManager} and enable the Impulse as the MIDI input
\begin{enumerate}
\item In the private section of \texttt{MainComponent} declare an instance of the class \texttt{AudioDeviceManager} (a good name for this object would be \texttt{audioDeviceManager}).
\item Now in the  \texttt{MainComponent} constructor we need to enable the Impulse MIDI input which can be achieved with the following call:
\begin{verbatim}
	//2 spaces between "Impulses"
	audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true); 
	
\end{verbatim}
\end{enumerate}

The method for receiving MIDI messages is directly analogous to the \texttt{TextButton} event and callback mechanism explored previously. MIDI messages are received on an active MIDI input port which then invokes a callback function within the application where the MIDI message is received and a corresponding response is triggered. 

\item To receive incoming MIDI messages we need to add a MIDI input callback function
\begin{enumerate}
\item Using what we learned last week, make the \texttt{MainComponent} inherit from \texttt{MidiInputCallback} 
\item Now override the pure \texttt{virtual} function \texttt{void handleIncomingMidiMessage (MidiInput*, const MidiMessage\&)} and add a \texttt{DBG()} message to this function so that you can see when messages are received.
\item Now add our \texttt{MainComponent} class to be a listener to the MIDI input by adding the following call to the \texttt{MainComponent} constructor
\begin{verbatim}
	audioDeviceManager.addMidiInputCallback (String(), this);
\end{verbatim}
\item Ensure that our \texttt{MainComponent} listener stops listening to the MIDI input when the program closes by adding the following to the \texttt{MainComponent} destructor:
\begin{verbatim}
	audioDeviceManager.removeMidiInputCallback (String()), this);
\end{verbatim}
\end{enumerate}

\end{enumerate}

Now all steps have been achieved to enable  the receipt of MIDI within our application where all messages are received by the function \texttt{handleIncomingMidiMessage()}. Run the program and ensure that you can see that MIDI messages are successfully received.

\subsection{Exercise: A MIDI Port Analyser}
Add a \texttt{Label} object called \texttt{midiLabel} to the interface of the application, add some test text to the label and ensure that you can see it. You may need to consult the API pages at\\ \url{https://docs.juce.com/master/classLabel.html} to clarify how to do this.

When the label is clearly visible add the following code to your \texttt{handleIncomingMidiMessage()} function:
\begin{verbatim}
    		String midiText;
    
    		if (message.isNoteOnOrOff())
   		 {
        			midiText << "NoteOn: Channel " << message.getChannel();
        			midiText << ":Number" << message.getNoteNumber();
        			midiText << ":Velocity" << message.getVelocity();
    		}
    
    		midiLabel.getTextValue() = midiText;
\end{verbatim}

Extend this code so that all types of MIDI messages available on the Impulse are identified and displayed with their values in the \texttt{midiLabel} component on the application interface. You should search the Juce API documentation for the class \texttt{MidiMessage} for functions like \texttt{isNoteOnOrOff()} which will enable you to identify the different MIDI message types. 


\subsubsection{Exercise: Adding MIDI Output}
We can also make our applications send MIDI messages. In our MIDI callback we will also send MIDI out to a virtual synthesiser called SimpleSynth. 

\begin{enumerate}
\item Launch SimpleSynth from the \texttt{/Applications/ExtraApps} folder. 
\item On the SimpleSynth MIDI Source change the drop down menu to `SimpleSynth virtual input'.
\item In the \texttt{MainComponent} constructor set the default MIDI output to SimpleSynth with the following line:
\begin{verbatim}
	audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
\end{verbatim}
\item In the \texttt{handleIncomingMidiMessage()} function forward all incoming messages to SimpleSynth with the call:
\begin{verbatim}
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (message);
\end{verbatim}
\end{enumerate}

\subsubsection{Exercise: MIDI Output Message Formatter}
Add the components that allow the user to enter any type of MIDI message via the interface with a `send' button that sends the message on the output MIDI port. You could use a slider with the style set to \texttt{IncDecButtons} that allows the user to enter/change the raw MIDI data as shown below.

\begin{center}
\resizebox{0.20\textwidth}{!}{\includegraphics{images/slider}}
\end{center} 

Alternatively (and even better) you could allow the status byte to be set with a \texttt{ComboBox} that sets the type of MIDI message (Note On, Control, Program, Pitch Bend, etc.) and a slider for the MIDI channel.

Perhaps your interface might resemble the following:
\begin{center}
\resizebox{0.65\textwidth}{!}{\includegraphics{images/interface}}
\end{center} 

\textbf{Hint}: To create MidiMessages in the correct format for testing the class \texttt{MidiMessage} has a number of \texttt{static} functions, for example \texttt{MidiMessage::noteOn}, \texttt{MidiMessage::controllerEvent}, \texttt{MidiMessage::programChange}, etc..

\subsubsection{Audio Functionality in Juce}
\label{sectionAudioFunc::image}
Audio input and output is also handled using a callback mechanism. This mechanism relies on a callback function that manages incoming and outgoing audio data at the same time: the incoming audio is received by the callback function and the outgoing audio is returned. Audio is received from an audio source and sent to an audio destination at a consistent rate; consequently, the audio callback is called at a constant rate to receive and supply the audio data. As audio samples are time critical and streamed at a high rate, typically 44.1kHz, it is impractical to invoke the callback function for individual audio samples. Consequently, the callback function is provided with two frames (pointers to arrays), one containing the input audio samples and the other which acts as a container for the output audio samples. 

\begin{center}
\includegraphics[width=0.45\textwidth]{images/audioFrames}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Exercise - Adding Audio Functionality in Juce}
Take another fork of the \texttt{Practical06b-JuceBasicWindow} repository found at  \url{https://gitlab.uwe.ac.uk/sda}. Rename your fork `Practical06b-BasicAudio' then clone this repository and ensure that it builds and runs.

Add audio support to the application:
\begin{enumerate}
\item As with the MIDI project add an \texttt{AudioDeviceManager} to your \texttt{MainComponent} class and initialise the audio I/O in the \texttt{MainComponent} constructor with the call:
\begin{verbatim}
	audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
\end{verbatim}

\item To process Audio messages we need to make \texttt{MainComponent} recive audio callbacks.
\begin{enumerate}
\item Make the \texttt{MainComponent} inherit from \texttt{AudioIODeviceCallback} 
\item Now override the pure \texttt{virtual} functions \texttt{audioDeviceIOCallback()} \\, \texttt{audioDeviceAboutToStart()} and \texttt{audioDeviceStopped()} and add a \texttt{DBG()} message to each so that you can see when they are called. 
\item Now register our \texttt{MainComponent} class to receive audio callbacks by adding the following call to the \texttt{MainComponent} constructor
\begin{verbatim}
	audioDeviceManager.addAudioCallback (this);
\end{verbatim}
\item Ensure that our \texttt{MainComponent} listener stops listening to the MIDI input when the program closes by adding the following to the \texttt{MainComponent} destructor:
\begin{verbatim}
	audioDeviceManager.removeAudioCallback (this);
\end{verbatim}
\end{enumerate}
\end{enumerate}

Now, to make an audio through program initially, we will copy directly from the audio input (mic) buffer to the audio output buffer (headphones) which is achieved as follows: (use headphones and watch out for feedback!!).
\begin{verbatim}
void MainComponent::audioDeviceIOCallback (const float** inputChannelData,
                            int numInputChannels,
                            float** outputChannelData,
                            int numOutputChannels,
                            int numSamples)
{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        *outL = *inL;
        *outR = *inR;
        
        inL++;
        outL++;
        outR++;
    }
}
\end{verbatim}

\subsubsection{Audio Callback Descriptions}
The callback functions for the \texttt{AudioIODeviceCallback} class are described below.

\paragraph{The \texttt{audioDeviceAboutToStart()} Function}
\begin{verbatim}void audioDeviceAboutToStart (AudioIODevice* device)\end{verbatim}
This function lets our object know that audio is about to start and repeated calls to the \texttt{audioDeviceIOCallback()} are about to commence. It is also called whenever the sample rate or frame size is changed. If the sample rate is of interest to your object it can be obtained with a call to \texttt{getCurrentSampleRate()} on the \texttt{device} object pointed to by the function's argument.

\paragraph{The \texttt{audioDeviceStopped()} Function}
\begin{verbatim}void audioDeviceStopped(void)\end{verbatim}
This function lets our object know that audio has stopped. This call allow any operations to be performed when callbacks to the \texttt{audioDeviceIOCallback( )} function have stopped.

\paragraph{The \texttt{audioDeviceIOCallback( )} Function}
\begin{verbatim}
void audioDeviceIOCallback ( const float** inputChannelData, 
                            int 	numInputChannels, 
                            float** outputChannelData, 
                            int numOutputChannels, 
                            int numSamples)
\end{verbatim}
This is the function where all the audio work is done, performing the process illustrated in section \ref{sectionAudioFunc::image}. It has many input arguments with some potentially confusing pointer syntax:
\begin{enumerate}
\item \texttt{inputChannelData}\\
This is the pointer through which all incoming audio on the connected audio port is accessed, this could be from the stereo line in or microphone input. Confusingly, this argument is a pointer to a pointer. If you study figure in section \ref{sectionAudioFunc::image} above the diagram shows a pointer to access an array of floats representing a single channel of an audio input stream. In reality audio devices have multiple channels so we need a pointer to an array of pointers. The pointers in the array each point to an array of floats. Each of these float arrays contain a frame of incoming audio data for an individual channel. This pointer relationship is illustrated for a 2 channel stereo input below. 
\begin{center}
\includegraphics[width=0.8\textwidth]{images/pointToPoint}
\end{center}

\item \texttt{numInputChannels}\\
This argument lets us know the number of pointers to channel data there are in the inputChannelData array. That is, the number of input channels there are active on the connected audio device. 

\item \texttt{outputChannelData}\\
This is the pointer to the outgoing audio frames which will be streamed to the connected audio output device. As with the \texttt{inputChannelData} argument, this is a pointer to a pointer. The structure is the same, there is an array of pointers that each point to an array of floats. The number of pointers matches the number of output channels and each pointer points to an array of floats in which the outgoing audio data must be written. 

\item \texttt{numOutputChannels}\\
This argument lets us know the number of pointers to separate arrays of channel data there are in the outputChannelData array. That is, the number of output channels there are on the connected audio device. 

\item \texttt{numSamples}\\
This argument lets us know the number of samples there are in each float array for each channel. This will match the size of the audio buffer specified on the interface to our application.

\end{enumerate}

\subsubsection{Exercise: Amplitude Control By \texttt{Slider}}
Add a slider to the interface of the application to control the amplitude of the program. 

\subsubsection{Exercise: Amplitude Control By MIDI}
Now add MIDI support to this application and write a program where the amplitude of the audio signal is controlled by the modulation wheel on the Impulse. Recall that amplitude should be in the range 0. - 1. and control values are in the range 0 - 127. 

\subsubsection{Exercise: MIDI Interface Control}
Now try to control the position of the slider on the interface from the modulation wheel on the Impulse. You should find that this causes the program to stop and drop into the debugger. We will talk about why this is next week as we explore the realm of multi threaded programming. But in the meanwhile instead of setting the value directly you should be able to get around this problem by calling:

\begin{verbatim}
slider.getValueObject().setValue (newValue); 
\end{verbatim}

\subsection{Encapsulating Interface Elements within A Single Component}
Last week you made your own button class and then encapsulated 5 of these buttons in your own \texttt{ButtonRow} component. It is always a good idea to identify collections of related GUI components and package these into a component class of their own. 

Reload the Basic MIDI `(practical06)' exercise completed earlier. Note that all of the MIDI message formatting components developed for this project earlier in the session are all related and thus constitute a good candidate for subsuming into a new \texttt{Component} class. This process is represented in the image below, where the \texttt{Label}s, \texttt{ComboBox} and \texttt{Slider}s are all contained within a new class called \texttt{MidiMessageComponent}, which derives from \texttt{Component}. The new \texttt{MidiMessageComponent} can then be added to our \texttt{MainComponent} as simply as adding a single button. 

\begin{center}
\resizebox{\textwidth}{!}{\includegraphics{images/interface2}}
\end{center}

\subsubsection{Exercise:\texttt{MidiMessageComponent} Class Implementation}
Create a new Juce class called \texttt{MidiMessageComponent} that inherits from the \texttt {Component} class and contains within it all the interface components that are used to construct the MIDI message (as shown in the image above). You should create \texttt{MidiMessageComponent.h} and \texttt{MidiMessageComponent.cpp} files to achieve this. Then replace all of the GUI components in \texttt{MainComponent} with a single instance of this new \texttt{MidiMessageComponent} class. Note that the \texttt{sendButton} should remain outside of this class. When you have the GUI aspects complete there are some additional steps to completing this process:

\begin{enumerate}
\item Now in \texttt{MainComponent} in the \texttt{buttonClicked()} function we would like to get the MIDI message from the \texttt{MidiMessageComponent} by calling a function called \texttt{getMidiMessage()} on our \texttt{MidiMessageComponent} rather than from the individual components in \texttt{MainComponent}, which should have been relocated.
\begin{enumerate}
\item Define \texttt{MidiMessageComponent::getMidiMessage()} such that it returns a \texttt{MidiMessage} reflecting the settings on its interface components. 
\item Back in the \texttt{buttonClicked()} function, send the \texttt{MidiMessage} that is returned by the \texttt{MidiMessageComponent}. 
\end{enumerate}
\texttt{MainComponent} will no longer need to inherit from \texttt{ComboBoxListener} and consequently the \texttt{MainComponent::comboBoxChanged()} function can also be removed.
\item You should now have an appreciation of how \texttt{Component} objects can be organised into a neat compositional hierarchy. 
\end{enumerate}

\subsection{Further Exercise}
Reload the Basic Audio `(practical06)' project and adapt it to implement one of the plugin effects that you created last year in the module APDI.

\subsection{Homework}
Finish any exercises that were not completed in the lab before next week.
