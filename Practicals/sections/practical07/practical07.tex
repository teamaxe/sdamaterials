\rule[.4cm]{\textwidth}{2pt}
\section{Practical 7 - UML and Threading}
\label{sec:prac07}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this practical we will explore class relationship visualisation with the Unified Modelling Language before learning about Threads. We will learn that multiple threads of execution enable tasks to take place `simultaneously' within a computer application. We will also learn about the mechanisms that ensure problem free interactions between different threads (thread safety). We will also introduce the Juce \texttt{Thread} classes for creating and using threads before developing a larger sequencing exercise using design patterns. On successful completion of this practical you will have an understanding of:

\begin{enumerate}
\item the concept of threading.
\item the threads at play within the Juce applications that we have met so far.
\item critical sections for accessing the shared data between separate threads.
\item program design and structural class relationship modelling with the Unified Modelling Language UML.
\item how to create your own threads.
\item basic design patterns.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{JuceBasicAudio Project}
Take a fork of the \texttt{Practical07-JuceBasicAudio} repository found at  \url{https://gitlab.uwe.ac.uk/sda}. Clone this repository with the rest of your SDA projects (remember it must be within the same folder as your Juce repository (but not in the Juce repository)). Build the project and ensure that it works - Watch out for audio feedback!

It is unsustainable for our \texttt{MainComponent} to do everything in our application so, as always, there are neater ways to divide programs up. This project shows a neater arrangement of the Juce project where GUI elements and Audio elements are separated into different parts of the program. It also shows how menu systems can be added to your programs, browse the source code to see how the project is structured. It is often useful to represent program designs and class structures using diagrams and the most standard method for representing program designs visually is the Unified Modelling Language (UML). UML is a comprehensive method for graphically representing computer programs and object orientated designs in a simple and readable way. The main classes of the Juce Basic Audio application are expressed by the UML class relationship diagram in Figure \ref{fig:umlbasicAudio}. 

\begin{figure}[!ht]
	\centering
		\includegraphics[width=0.9\textwidth]{images/AudioUML}
	\caption{Juce Basic Audio Class Relationship Diagram}
	\label{fig:umlbasicAudio}
\end{figure}

\subsection{The Unified Modelling Language UML - Class Relationship Diagrams}
Graphical representations of software are very useful for conveying program structure and class relationship information. The structure of a software system can be understood much more quickly by studying diagrams than reading documentation or studying source code. In the early years of object orientated programming (OOP), designers would develop their own notation for the structure which was believed to slow the adoption of OOP as a programming choice, so in 1997 the UML was proposed as a non-proprietary, standardised, object orientated graphical design notation.

\subsubsection{Class Diagrams}

\begin{figure}[htbp]
	\centering
		\includegraphics[width=0.8\textwidth]{images/classdiagram}
	\caption{Class diagram}
	\label{fig:classdiagram}
\end{figure}

UML includes many diagram types but the Class Relationship Diagram is the most relevant and important for this module. Class relationship diagrams provide a box to represent each class. Often the class representation is a single box containing only the name of the class (Figure \ref{fig:classdiagram} left), although in more detailed diagrams the box is often divided into three parts (Figure \ref{fig:classdiagram} right) to show the \textbf{class name} (top), the \textbf{attributes} (centre) and the \textbf{member functions} (bottom). 

\textbf{Attributes} are properties of the object which are visible externally; for example, in the \texttt{Oscillator} class above, the externally visible attributes are frequency and waveform. The attributes express the elements of the class that the user might be concerned with, however, internally this data might be organised in a different way (recall the Time example from the lecture part of the session). 

\textbf{Member functions} are the functions that form the public interface to the class.

It is not necessary to list all attributes and member functions as large classes would be huge and it would be impractical to represent large programs with this level of detail. It is much more usual only to list the name of the class in a box as shown in Figure \ref{fig:classdiagram} left.

\subsubsection{Class Relationships}
In a software system, classes have relationships that enable the program to achieve its task. These relationships are represented by connecting `wires' between class boxes, where the ends of the wires convey the type of relationship. The different connectors and their relationship meaning are shown in Figure \ref{fig:umlconnectors}.

\begin{figure}[!ht]
	\centering
		\includegraphics[width=0.5\textwidth]{images/umlconnectors}
	\caption{UML Connectors}
	\label{fig:umlconnectors}
\end{figure}

The meaning of these relationships is provided in the table below.
\begin{table}[!ht]
\centering
\begin{tabular}{ l p{12cm}} \hline
Relationship 	& Meaning	\\ \hline 
Inheritance	& As we have seen previously, inheritance is when a general base class is inherited and extended by a derived class and indicates an `\textit{is a}' relationship.\\
Composition	& Composition indicates a `\textit{has a}' relationship, when a class contains an object (or objects) of another class and these objects are created and destroyed by the containing class\\
Aggregation 	& Aggregation like composition indicates a `\textit{has a}' relationship but the contained object has an existence outside of this relationship, often the object is created and destroyed in another class but there is a reference to it in this class.\\
Dependency	& This represents any other types of relationship. \\
%Association	& Associations also indicate a `has-a' relationship but do not specify the strength of the association as specifically as aggregation and composition do. Neither does the association indicate the containing class. This is considered a more general relationship indicator.\\
%Directed Association & As above this connector indicates and an association and also indicates the containing class. \\
\end{tabular}
\caption{Relationship Connectors}
\label{tab:multiplicity}
\end{table}

The connections are therefore used as follows:
\begin{figure}[!ht]
	\centering
		\includegraphics[width=0.8\textwidth]{images/umlconnectorRelationships}
	\caption{UML Connection Relationships}
	\label{fig:umlconnectorsrelationship}
\end{figure}

Ranges of quantities can also be specified next to the connection if the quantity is dynamic and controlled at run time, common choices are shown in Table. \ref{tab:multiplicity}.

\begin{table}[!ht]
\centering
\begin{tabular}{|l|c|} \hline
Quantity 	& Symbol	\\ \hline \hline
any number	& * 	\\ \hline
one or more	& 1..*\\ \hline
zero to two	& 0..2\\ \hline
\end{tabular}
\caption{Multiplicity Indicators}
\label{tab:multiplicity}
\end{table}

Now refer back to the class diagram for the JuceBasicAudio application to see how the GUI and Audio are related. Note that the \texttt{MainComponent} is contained within the window but also includes a reference to the \texttt{Audio} object. Look at the code to see how these relationships are expressed in code.

\subsection{Parallel Computation}
In modern computer systems many different computational tasks execute simultaneously. At the most obvious level, we run multiple applications in order to write an assignment, listen to music and check your email at the same time. Within each of these applications there are further examples of concurrent execution. For example, within a browser one tab of content may streaming music while in another tab you simulataniously click links on blackboard. 

It is parallel computation that facilitates the execution of multiple concurrent jobs and there are mechanisms available to programmers that enable \textit{processes} or \textit{threads} to be spawned to take care of separate tasks. The application provided at the beginning of this program incorporates parallel computation: a program that enables the user to click on the File menu while audio from the mic input is sent to the speaker output. One thread of execution handles the GUI interaction and the other handles the audio processing. This way, \textbf{GUI animations and rendering do not block the flow of audio data and vice-versa}. 

\subsubsection{Processes and Threads}
Within an application, parallel computation is achieved by spawning multiple processes and/or threads. We will only concern ourselves with threads for now but this section will provide a very brief description of both, so that differences may be understood.

\paragraph{Processes}
A process is a complete unit of execution that occupies its own space in memory. A process represents a section of the complete application that has been identified as a self-contained unit that can be run as a process. The process can then communicate with other processes through interprocess connection mechanisms like a file, a pipe or a socket. 

\paragraph{Threads}
Unlike processes, threads share memory space which means that no formal inter-thread communication mechanism is required: multiple threads can access a shared set of data variables/objects. However, with multithreaded applications, precautions must be taken to ensure that errors do not occur due to the simultaneous access of shared data at the same time, e.g., one thread reads some data while another is in the middle of writing it). Throughout the remainder of this practical the specifics of threads and shared data will be explored in greater detail.

\subsection{Threads}
On a computer with a single-core processor it is not possible to execute more than one instruction simultaneously, threading here is achieved by the operating system switching execution between different threads fast enough to give the perception of simultaneous execution. On multi-core processors the operating system may choose to allocate separate threads to separate cores to enable true simultaneous execution. 

When an executable is launched it exists as a single process. Within this process there is at least one thread of execution. \texttt{main()} represents the executable code of the main thread. Within \texttt{main()}, further calls can be made that spawn further threads of execution. The execution of the main thread will then continue in parallel with the spawned threads. This mechanism is illustrated below for two threads running in parallel where thread 1 is the main thread and thread 2 is the spawned thread.

\begin{center}
\includegraphics[width=0.5\textwidth]{images/sharedMem}
\label{fig:sharedMem}
\end{center}

\subsection{Juce Threads}
When building Audio and MIDI applications with Juce there is a thread to deal with the audio events, a thread for the MIDI events and a thread for the GUI (and other) events. These threads are named the \textbf{audio}, \textbf{MIDI} and \textbf{message} threads respectively. 

\paragraph{Message Thread}
When the window is resized or an event is invoked via a user interface component (e.g. \texttt{Button}, \texttt{Slider}, \texttt{ComboBox}, etc.) the corresponding callback functions are called on the message thread. 

\paragraph{MIDI Thread}
When a class is registered to receive MIDI messages, the \texttt{handleIncomingMidiMessage()} function is called on the MIDI thread whenever a MIDI message is received on the connected port(s).

\paragraph{Audio Thread}
Similarly, when a class is registered to receive audio messages the \texttt{audioDeviceIOCallback()} function is called on the audio thread whenever a frame of samples has been received on the audio input and is required on the audio output. 

Consequently, it is possible to identify on which thread each of the functions within \texttt{MainComponent} will be called and identify which variables are shared.

\paragraph{Thread Priority}
When threads are spawned, their priority can be specified explicitly. Naturally the audio and MIDI threads are high priority as audio applications must handle MIDI and audio as promptly as possible. To minimise this latency it is desirable to spend as little time as possible in the \texttt{handleIncomingMidiMessage()} and \texttt{audioDeviceIOCallback()} functions. While you are doing something in the MIDI thread, no further MIDI messages can be received. Similarly, while you are doing something in the audio thread no further audio data can be processed. 

Audio is extremely time critical, any delays in the audio thread are immediately heard on the audio output as glitches or other audio artefacts which can have devastating consequences in venues with large PA systems, so every effort should be made to minimise the likelihood of glitches. The message thread is less time critical. A delay in the update of an interface component of one 50th of a second is imperceptible, a similar delay in the audio thread produces a glitch. 

Consequently, it is important to be mindful of the threads on which events are executed and structure the program accordingly. 

\paragraph{Inter-Thread Communication}
There are many circumstances when the events on one thread might affect the behaviour of another. For example a MIDI note message received on the MIDI thread could cause the frequency of a note being synthesised on the audio thread to change. Similarly, the audio signal received on the audio thread might update a level meter on the interface or a note message received on the MIDI thread might update a \texttt{Slider} on our interface. It is possible to flag an event in the audio/MIDI thread that invokes a response by the message thread, thus removing low priority computation from high priority threads. We will learn how to do this in future practicals. First lets return to our starting point code, fix the error and then build a synthesiser.

\subsubsection{Exercise - Gain Control}
As a start, add a slider to the interface of the application and use it to control the gain of the mic signal passed to the speakers.

\subsubsection{Exercise - SineSynth}
In this exercise we will build a simple sine wave synthesiser controlled by MIDI. Last year in APDI you built a very basic sinusoidal oscillator in your plugin. You are about to reimplement this process here.

Here is an adapted description from the AP practical:

In Xcode, find the file \texttt{Audio.h}. In the editor, find the line marked ``\texttt{private:}'' and add the following declarations:

{\small
\begin{verbatim}
   float frequency;
   float phasePosition;
   float sampleRate;
\end{verbatim}}

Next, open the file \texttt{Audio.cpp} in the main project window to open the editor and find the function \texttt{audioDeviceAboutToStart()}.  Add the following variable initialisations:

{\small
\begin{verbatim}
    frequency = 440.f;
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();
\end{verbatim}}

This will ensure that our new member variables are initialised when the audio processing begins.  

Find the function \texttt{audioDeviceIOCallback()} and add the following lines \textbf{before} the \texttt{while} loop:

{\small
\begin{verbatim}
const float twoPi = 2 * M_PI;
const float phaseIncrement = (twoPi * frequency)/sampleRate;
\end{verbatim}}

In function \texttt{audioDeviceIOCallback()}, inside the main \texttt{while} loop:

\begin{enumerate}
\item Increase the value of \texttt{phasePosition} by \texttt{phaseIncrement}

\item If \texttt{phasePosition} is greater than \texttt{twoPi}, then subtract \texttt{twoPi} from the value, to ensure it stays in the 0 to $2\pi$ range.  

\item Use the \texttt{sin()} function to convert the current phase value into the sine of that value.  Assign this result to both the left and right output using the pointers. 
\end{enumerate}

Build the application and ensure that you can hear a 440Hz sine wave.  

Now set the \texttt{frequency} variable using note numbers arriving through the \texttt{handleIncomingMidiMessage()} callback. You will find the function \texttt{MidiMessage::getMidiNoteInHertz()} useful for this. You should now have the beginnings of a basic synthesiser.

\subsection{Shared Data - Simultaneous Access By Two Threads}
The diagram on page \pageref{fig:sharedMem} shows an inner circle that represents the data shared by the two threads. In the synthesiser solution developed above, the shared data represents the \texttt{gain}, and \texttt{frequency} variables. The former is controlled in the \textbf{message} thread when the slider is moved, and both are accessed in the \textbf{MIDI} and \textbf{audio} threads in the callbacks \texttt{handleIncomingMidiMessage()} and \texttt{audioDeviceIOCallback()} respectively. When data is shared between threads in this way, problems can arise because one thread could be in the process of reading a shared memory object when the other thread is part way through a write. The outcome can be unpredictable and could result in a glitch or in some cases a crash. Currently the program is not `thread safe' and even though it work fine most of the time, at some point a simultaneous read/write will occur. Cosenquently, threading errors are very intermittent and hard to debug, so its essential that we design programs that anticipate these problems and put in place measures to ensure they never occur.  There are many methods to manage safe communication between threads, in this instance our problems can be solved by using \texttt{Atomic}s which is an excellent (`lock free') method used frequently within audio applications.

\subsubsection{\texttt{Atomic}s}
Juce includes a helpful class that enables the safe access to variables by multiple threads. The class is called \texttt{Atomic} which is a template class for storing fundamental types like \texttt{ints}, \texttt{longs}, \texttt{floats}, \texttt{doubles}, etc, on which `atomic' operations can be performed. An atomic operation allows changes to values on one thread that appear instantaneous to other threads, i.e., it is guaranteed that reading threads will never be able to observe a value mid way through a change in a writing thread. 

An atomic \texttt{float} may be declared as follows:
\begin{verbatim}
   Atomic<float> frequency;
\end{verbatim}

Writes can be made to the variable as normal using the \texttt{=} operator:
\begin{verbatim}
   frequency = message.getMidiNoteInHertz (message.getNoteNumber());
\end{verbatim}

and a read can be made by calling the \texttt{get()} member function:
\begin{verbatim}
   frequency.get();
\end{verbatim}

\subsubsection{Exercise: Thread Safe Oscillator}
In your program replace variables \texttt{gain} and \texttt{frequency} with \texttt{Atomic}s to make the \textbf{SineSynth} `thread safe'. Test that everything works as expected.

\subsection{Exercise Object Orientated Design: Oscillator Class}
Hopefully, with your object orientated hat on, you will have thought to yourself ``this oscillator code should really be separated from the rest of the \texttt{Audio} code, and should be in a class of its own''. 

Design and implement a \texttt{SinOscillator} class that encapsulates the complexity of our oscillator. The class interface should include at least three functions, in addition to the constructor and destructor:
\begin{enumerate}
 \item \texttt{getSample()} which should return the next sine wave sample.
 \item \texttt{setFrequency()} which should update the oscillator frequency.
 \item \texttt{setAmplitude} which should update the oscillator amplitude.
 \end {enumerate}

Create files for the \texttt{SinOscillator} class and add an instance of the new \texttt{SinOscillator} class to \texttt{MainComponent} in place of your previous oscillator code and make sure that it works. Do not add atomics to the \texttt{SinOscillator} leave these variables external to the class and feed them in only in the Audio thread - your sin oscillator should only ever be accessed in the audio thread. Also lose the gain slider and control the oscillator by the note velocity.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Threads}

Code written so far runs in one of three threads: the message, audio or MIDI threads. It is also possible to create our own threads using Juce. 

\subsubsection{Exercise - Creating New Threads}
\begin{enumerate}
\item In your \texttt{MainComponent} class inherit from the class \texttt{Thread} and override the function \texttt{run}.

\item Change the first line of the constructor definition to initialise the tread with a name:
\begin{verbatim}
MainComponent::MainComponent (Audio& a) :  Thread ("CounterThread"),
                                           audio (a) \end{verbatim}

\item In the \texttt{MainComponent} constructor add the code:
\begin{verbatim}
	startThread();
 \end{verbatim}

\item In the \texttt{MainComponent} destructor add the code
\begin{verbatim}
 	stopThread(500);
\end{verbatim}


\item In the \texttt{run()} function. Insert the following code. 
\begin{verbatim}
while (!threadShouldExit())
{
	   uint32 time = Time::getMillisecondCounter();
	   std::cout << "Counter:" << Time::getApproximateMillisecondCounter() << "\n";
	   Time::waitForMillisecondCounter (time + 100);
}
\end{verbatim}

\end{enumerate}

\subsection{The Juce Thread Class}
As you will have noted, the thread can be started with a call to the function \texttt{startThread()}, which returns immediately after the \texttt{run()} function has been initiated in a new and separate thread. In this thread \texttt{run()} behaves like a \texttt{main()} function that runs in parallel with other threads - it will run until execution hits the end of the function. Consequently, a loop should be placed within \texttt{run()} that repeatedly checks the function \texttt{threadShouldExit()}. If the call to \texttt{threadShouldExit()} returns \texttt{true} the loop should stop.  

Threads can then be terminated simply by calling the function \texttt{stopThread (500)}, which results in the function \texttt{threadShouldExit()} returning true, and then waits for 500ms for the run function to exit. Study the \texttt{MainComponent} class and look for the code that facilitates the \texttt{Thread} class inheritance (notice the \texttt{Thread} initialiser on the \texttt{MainComponent()} constructor: \texttt{Thread ("CounterThread")}). Also find the calls to the functions \texttt{startThread()}, \texttt{stopThread (500)} and how \texttt{threadShouldExit()} is used in the \texttt{run()} function. 

This program uses a fairly accurate method for printing text using Juce's \texttt{Time} functions every 200ms.  Astute readers might note that this behaviour could be achieved using the \texttt{Timer} class employed in previous exercises, but there is an important difference. The \texttt{Timer} class runs in the message thread, which means that other activity in this thread could result in timing inaccuracies. This would not be a problem for non-time critical processes such as painting/updating the GUI but for timing clock intervals in a music sequencing application this could lead to noticeable timing errors. By placing a timer process in a dedicated thread, greater accuracy can be achieved.

\subsubsection{Exercise: Starting and Stopping the Counter}
Add a button to the interface and use it to start and stop the counter. Update the toggle state of the button from the state of the counter with a call to \texttt{isRunning()};

\subsubsection{Exercise: An Accurate Counter}
Modify the program such that when start is pressed, a counter is incremented from zero and printed to the console in the \texttt{run()} function. When the thread is stopped the counter should be reset. 

Now create and implement a new and separate class called \texttt{Counter} that encapsulates the thread and the counter. Test that the application works as it did before.

\subsubsection{Creating a Counter Listener}
A powerful design pattern is the \textit{broadcaster listener pattern} which is used widely throughout Juce. This is where listener objects can be registered to receive messages from broadcaster objects, and respond to changes accordingly. An example is the \texttt{Button} and \texttt{Button::Listener} arrangement which allows a \texttt{Button} to communicate with your class through the \texttt{Button::Listener} interface. 

We will now turn our \texttt{Counter} class into a broadcaster and define a \texttt{Counter::Listener} class interface so that other objects within our program can receive counter messages. Follow these steps:

\begin{enumerate}
\item In the \texttt{Counter} class declare a nested pure virtual base class called \texttt{Listener} for all listener classes to inherit as follows:
\begin{verbatim}
    /** Class for counter listeners to inherit */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener()  {}
        
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (unsigned int counterValue) = 0;
    };
\end{verbatim}
\item Now declare a pointer to one of these \texttt{Listener}s in the private section of \texttt{Counter} as follows:
\begin{verbatim}
    Listener* listener;
\end{verbatim}
\item In the \texttt{Counter} constructor initialise the \texttt{listener} pointer to \texttt{nullptr}.
\item Add the function \texttt{setListerner()} defined as follows:
\begin{verbatim}
    void setListener (Listener* newListener)
    {
        listener = newListener;
    }
\end{verbatim}
\item Now, in the run function, call the \texttt{counterChanged()} on the listener object as follows:

\begin{verbatim}
void Counter::run()
{
    while (!threadShouldExit()) 
    {
        uint32 time = Time::getMillisecondCounter();
        if (listener != nullptr)
            listener->counterChanged (counterValue++);
        Time::waitForMillisecondCounter (time + 200);
    }
}
\end{verbatim}
\item Now add \texttt{MainComponent} to listen to the \texttt{Counter} and print the \texttt{counterValue} to the console.
\end{enumerate}  

\subsubsection{Exercise: Percussion Metronome}
Implement a function in your \texttt{Audio} class clalled \texttt{beep()} which will play a 440 Hz sine wave for 100 ms. Now use the counter to trigger the beeps so that you construct a metronome. Add a BPM control to the sequencer. Make sure that all inter thread communications use atomics to ensure that they are thread safe.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Finishing up}

Make sure that all the lines of code that have been produced are suitably recorded in a text file.  