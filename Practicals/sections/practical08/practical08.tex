\rule[.4cm]{\textwidth}{2pt}
\section{Practical 8 - Designing Base Classes and Generating Documentation}
\label{sec:prac08}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In previous SDA sessions we have inherited from a selection of different Juce classes (e.g. \texttt{Component}, \texttt{MidiInputCallback}, \texttt{AudioIODeviceCallback}, \texttt{Button::Listener}, etc.), which have simplified and accelerated our development. This week, we will begin to examine inheritance further by developing our own base classes from which other classes can derive. We will then finish up with an introduction to Doxygen, which will help you to generate documentation for your code automatically. On successful completion of this practical you should have an understanding of: 

\begin{enumerate}
\item inheritance hierarchies and base/derived class relationships.
\item the use of the \texttt{virtual} keyword to enable base class functions to be overridden.
\item how to create abstract base classes with pure virtual functions.
\item how to generate documentation automatically from our source code.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Exercise - Getting Started}
In this session we will progressively build an \texttt{Oscillator} base class from the work that we created last week. However, we must begin with the same class for the descriptions to make sense. Before starting, make sure that all the code that you developed last week has been committed. Nowe create a new repository for this week called `Practical08 - Basic Audio' on your \url{https://gitlab.uwe.ac.uk/}{gitlab} account, clone it, put a copy of last weeks juce project and files into this repository and commit and push the changes to GitLab. Now modify the code so that it works like a very simple monophonic synthesiser but replace your own \texttt{SinOscillator} class with the \texttt{SinOscillator} class that is provided in this week's practical folder on \texttt{StudnentShared}. The class will be very similar, but there are a few minor differences that are important for today's work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Inheritance: Base Classes}
In previous practical sessions we have written derived classes that inherit and specialise/extend the features of a base class. This process was explored when a button click was tied to a specific action:

\begin{enumerate}
\item a class inheriting \texttt{Button::Listener} was declared.
\item the function \texttt{buttonClicked()} was defined implementing the process that should execute in the event of a button press.
\item an object of this class was registered as a listener to a button.
\end{enumerate}

Derived classes were also used when we started to develop applications with audio and MIDI capabilities by inheriting from the \texttt{AudioIODeviceCallback} and \texttt{MidiInputCallback} classes respectively. This process we referred to as client inheritance, where we developed derived classes inheriting from previously declared base classes. However, as we write increasingly specialised and complicated programs we may, identify a need to write our own base classes from which other classes will derive. This becomes particularly useful in applications where we are required to develop several classes that share certain functions and attributes.

In the initial description of inheritance relationships, the \texttt{Oscillator} class was used as a simple and clear example of an inheritance relationship. We may wish to implement several oscillator classes that each synthesise a different wave shape, the structure of each class would be identical with the exception of the generated wave shape. Each oscillator class would have the same attributes: frequency, amplitude, phase, and so fourth. Consequently, it makes sense to place these common attributes and functions into a base class from which each derived class will inherit. Derived classes need only implement the function that generates the wave shape inheriting all the common features from the base class. 

Study the \texttt{SinOscillator} class. In \texttt{SinOscillator.h} the interface lists the range of functions used to configure the oscillator and return the current waveform value. All of these functions would be the same for almost any type of oscillator with the exception of the function \texttt{renderWaveShape()}, which is the only function that would need to be modified to generate different waveform shapes. 

If further oscillators were to be declared, one approach would be to copy the entire class and make the necessary changes to the \texttt{renderWaveShape()} function. However, there would be much duplication in the project as all the other functions are repeated within each class. A much more appropriate and elegant solution would be to use the \texttt{SinOscillator} class as a base class from which new oscillator classes inherit, implementing (\textit{overriding}) the \texttt{renderWaveShape()} function to generate the new waveshape. 

Recall that the mechanism for overriding functions is connected with the keyword \texttt{virtual}. Any function declared \texttt{virtual} in the base class can be overridden in the derived class. 

\subsubsection{Exercise: Implementing A Square Wave Oscillator Using Inheritance}
Write a square wave oscillator class that inherits from \texttt{SinOscillator}. To do this you must make the necessary changes to the \texttt{SinOscillator} class enabling the \texttt{renderWaveShape()} function to be overridden. 

First, find the declaration for the function \texttt{renderWaveShape()} in \texttt{SinOscillator.h} and precede the declaration with the keyword \texttt{virtual}. The declaration should now be as follows:
\begin{verbatim}
   virtual float renderWaveShape(const float currentPhase);
\end{verbatim}
For reasons we will discuss in the lab, the destructor must also be declared virtual. Find the declaration for $\sim$\texttt{SinOscillator()} in \texttt{SinOscillator.h} and modify the declaration to match the following:
 \begin{center}
   \texttt{virtual} $\sim$\texttt{SinOscillator();}
\end{center}

That's it! Now we can write new classes inheriting \texttt{SinOscillator} that overrides \texttt{renderWaveShape()} to produce different wave shapes. Have a look at the function declarations for \texttt{nextSample()} in \texttt{SinOscillator.cpp}. Note how the phase is passed into \texttt{renderWaveShape()} to obtain the amplitude value for the current phase before the phase value is incremented. This relationship between the functions has been specially designed so that \texttt{renderWaveShape()} is passed the current phase value as an argument so that derived classes need not worry about incrementing any phase values, the only concern is the conversion of the \texttt{currentPhase} value to an amplitude value. 

In fact, the derived class is not permitted to access the private members of the base class. If access to some or all of the base class member variables is required those members can be declared \texttt{protected} rather than \texttt{private}, as will be described in the lecture accompanying this practical session.  

When the above changes have been made, we can write a \texttt{SquareOscillator} class inheriting\\ \texttt{SinOscillator}.
\begin{enumerate}
\item Create a new set of files called \texttt{SquareOscillator.h} and \texttt{SquareOscillator.cpp} and write a new class called \texttt{SquareOscillator}. 
\item The file \texttt{SquareOscillator.h} should begin with the preprocessor command:
\begin{verbatim}
   #pragma once
\end{verbatim}

\item This class should then inherit \texttt{SinOscillator} so the header file \texttt{SinOscillator.h} will need to be included. (hint: To derive a class from a base class, we use a colon (:) in the declaration of the derived class).
\item Declare and define the overriding \texttt{renderWaveShape()} function in the\\ \texttt{SquareOscillator} class and remember to use the keyword \texttt{override}. You will have to think through the logic of how to do this and remember to use the \texttt{override} keyword. You may even consider extending the class to enable duty cycle control.
\item Now in your \texttt{Audio} class, include \texttt{SquareOscillator.h} and change the previously declared\\ \texttt{SinOscillator} object to a \texttt{SquareOscillator}. No further changes should be necessary.
\item The application should now produce a square wave when notes are played. Note that this is a very simple method for generating a wave shape, you may be able to hear distortion in the tone due to aliasing as this is not a band limited squarewave oscillator. 
\end{enumerate}

Thanks to inheritance, it becomes very easy to create new oscillators that only implement the function that renders the wave shape the rest of the classes features are inherited from the base class as is. For example,  \texttt{setFrequency()} and \texttt{setAmplitude()} can be called on any object of type \texttt{SquareOscillator}, as they are inherited from \texttt{SinOscillator}. This approach speeds up development, reduces bugs and code duplication as subsequent changes/improvements to the implementation need only be added to the base class where they are inherited by all derived classes without having to duplicate any code. Moreover, inheritance brings us great flexibility: as all oscillators conform to exactly the same \textbf{interface} the oscillators are completely interchangeable without having to modify anything but the object declaration.

\subsection{Abstract Classes With Pure Virtual Functions}
In the examples created above, \texttt{SinOscillator} is the base class and \texttt{SquareOscillator} is the derived class. The inheritance hierarchy is illustrated below:

\begin{center}
\includegraphics[width=0.2\textwidth]{images/sinbase}
\end{center}

The connecting arrow indicates inheritance in the unified modelling language (UML). However, this is not an ideal relationship as intuitively \texttt{SinOscillator} and \texttt{SquareOscillator} should sit at the same level in the hierarchy. A more appropriate inheritance relationship is shown below:

\begin{center}
\includegraphics[width=0.4\textwidth]{images/oscbase}
\end{center}

Where both the \texttt{SinOscillator} and \texttt{SquareOscillator} are derived from the base class \texttt{Oscillator}. In this arrangement \texttt{Oscillator} is the class that implements the mechanics of an abstract oscillator but it does not specify a wave shape; the wave shape will be implemented within \texttt{Oscillator}'s derived classes. 

In this scenario \texttt{Oscillator} is an incomplete class - the \texttt{renderWaveShape()} function is declared but not implemented; its purpose is to declare the interface, not the actual waveshape. It is not possible to create objects of type \texttt{Oscillator}, it can only be used as a base class for derived classes that complete the \texttt{Oscillator} by implementing the \texttt{renderWaveShape()} function with a specfic waveshape. Classes like \texttt{Oscillator} exist only for the purposes of inheritance are called \textit{abstract base classes}, they must be completed by a derived class.

In C++, a class is abstract when one or more of its virtual functions are \textit{pure}. A \textit{pure virtual function} has `\texttt{= 0}' after its declaration and it does not have a definition. For example, the following is an example of a pure virtual function declaration:

\begin{verbatim}
   virtual float renderWaveShape(float currentPhase) = 0;
\end{verbatim}

This pure virtual declaration ensures that the \texttt{renderWaveShape()} function must be overridden in any derived classes inheriting \texttt{Oscillator}. Failure to implement a pure virtual function often produce errors resembling the following: `Field type XXX is an abstract class.'  Clicking to expand the error reveals further details to exam plain exactly what's wrong `Unimplemented pure virtual method \textit{VitualFunction} in \textit{DerivedClass}'.

\subsubsection{Exercise: Implementing the Oscillator Abstract Class}
Implement the class hierarchy shown in the previous diagram. The steps to achieving this are as follows:
\begin{enumerate}
\item Create a new \texttt{.cpp} and \texttt{.h} file for the new base class \texttt{Oscillator}.
\item Copy the content of the \texttt{SinOscillator} class files and paste them into the \texttt{Oscillator} class files.
\item In the \texttt{Oscillator} source files, replace all mentions of \texttt{SinOscillator} with \texttt{Oscillator} (including the preprocessor commands!).
\item Declare the \texttt{renderWaveShape()} function to be pure virtual in \texttt{Oscillator.h} and delete the implementation (definition) of this function from \texttt{Oscillator.cpp}. The \texttt{Oscillator} class is ready.
\item Modify the \texttt{SquareOscillator} so that it includes \texttt{Oscillator.h} (instead of \texttt{SinOscillator.h}) and inherits from \texttt{Oscillator} (instead of \texttt{SinOscillator}).
\item Return to \texttt{SinOscillator} and modify the class so that it inherits from \texttt{Oscillator}. This will involve the removal of all but the \texttt{renderWaveShape()} function - use the \texttt{SquareOscillator} class as a template for this if you are unsure.
\item Test both oscillators to ensure that they work.
\end{enumerate}

\subsection{Polymorphic Behaviour}
When inheritance is structured in this way, the beauty of polymorphism can be utilised. The inheritance relationship shown in the previous diagram indicates that both \texttt{SinOscillator} and \texttt{SquareOscillator} are objects of type \texttt{Oscillator}. This means that we can create a pointer to an \texttt{Oscillator} and set it to point to any object that derives from \texttt{Oscillator}: objects of type\\ \texttt{SinOscillator} and \texttt{SquareOscillator} in this instance. Calling the \texttt{renderWaveShape()} function on this pointer will execute the overridden function in the derived class for the object to which the pointer points. This means that the outcome of the \texttt{renderWaveShape()} call depends on where the pointer is directed, and consequently could result in the production of a variety of different wave shapes. This is polymorphic behaviour.

\subsubsection{Exercise: A Polymorphic Synthesiser}
Modify the previous exercise such that an instance of \texttt{SinOscillator} and \texttt{SquareOscillator} are included as members to the \texttt{AppComponent} class. Also declare a pointer of type \texttt{Oscillator} and in the constructor set it to point to the \texttt{SinOscillator} object. All oscillator functions should now be called on this pointer rather than directly on the objects.

Now add a combo box to the interface of the application that enables the waveform to be switched between the sine and square waveforms by directing the oscillator pointer to the appropriate oscillator object. You should consider threading and shared memory when completing this exercise. Because the pointer change will happen in the message thread you should make the change thread safe by making the pointer to \texttt{Oscillator} \texttt{Atomic}. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Doxygen: Using Doxygen to document Juce-based projects}

The following guidance is currently Mac-only but Doxygen is compatible with Windows.

Juce is documented using Doxygen which is a documentation system for many different programming languages. The main page for Doxygen can be found here:\\
\url{http://www.stack.nl/~dimitri/doxygen/}

To document code, formatted comments are used within the code. The special formatting can be kept to a minimum (and is thus more readable in the code) with some minor customisation of the default settings.

Doxygen is already installed in the UWE labs, however you may wish to use it at home, in which case it can be it can be downloaded from here:\\
\url{http://www.stack.nl/~dimitri/doxygen/download.html#latestsrc}

\subsubsection{Doxygen Commands}
To use Doxygen, your code must be commented with correctly formatted commands which are identified by Doxygen when it parses the source code as it generates the documentation for your project. The project we have developed provided for today's practical has been commented with the most common Doxygen commands. Navigate to the file \texttt{SinOscillator.h} and study the comments. You will notice comments above the class declarations and then above each member function declaration.

The commands are tabulated below:
\begin{center}
\begin{tabular}{l l } 
\texttt{/**} & This command indicates the start of a Doxygen comment block which will finish at the \\
& subsequent \texttt{*/}. With the Doxygen options that we will be using later, the first sentence \\
& (upto the first full stop) is used as the ``brief'' description, and everything following that \\
& is the ``detailed'' description. As we will see later. \\\\
\texttt{@see} & This command produces a cross reference to other related classes. In the documentation\\
& produced these classes are listed as useful links to other, related classes.\\\\
\texttt{@param} & This command enables descriptions for function arguments to be included. The \\
& command is followed by the argument name and its corresponding description.\\\\
\texttt{@return} & This command enables descriptions for function return value to be included. The \\
& command is followed by the return type description.\\
\end{tabular}
\end{center}

\subsubsection{Generating Documentation}
To generate documentation for this project follow these steps:
\begin{enumerate}
\item In the \texttt{/Applications/Extra Apps} folder and launch the Doxygen application.
\item Make sure the \textbf{Wizard} tab is selected.
\item In the \textbf{Topics} field, select \textbf{Project}:
	\begin{itemize}
	\item Enter the project name: \texttt{MonoSynth}
	\item Enter a basic project synopsis - i.e., `simple Monophonic Synthesiser', or similar.
	\item In the \textbf{Specify the directory to scan for source code} section select the \texttt{/Source} folder in the Xcode project (check the \textbf{Scan recursively} option.
	\item In the \textbf{Specify the directory where Doxygen should put the generated documentation} section, create and select \texttt{/Doc} folder next to the \texttt{Source} folder in the main project.
	\end{itemize}
\item In the \textbf{Topics} field, select \textbf{Mode}:
	\begin{itemize}
	\item Set the desired extraction mode to \textbf{All Entities} (this means everything gets "documented" even if you don't add documentation comments).
	\end{itemize}
\item In the \textbf{Topics} field, select \textbf{Output}:
	\begin{itemize}
	\item Switch `LaTeX' off. If you are interested in producing LaTeX files you may wish to switch this back on at a later stage.
	\item Switch `Rich Text Format (RTF)' on.
	\end{itemize}
\item In the \textbf{Topics} field, select \textbf{Diagrams}:
	\begin{itemize}
	\item Enusre that `Use built-in class diagram generator' is selected.
	\end{itemize}
\item In the tabs at the top of the interface select \textbf{Expert}:
\item In the \textbf{Topics} field, select \textbf{Project}:
	\begin{itemize}
	\item Scroll down and turn on the \texttt{JAVADOC\_AUTOBRIEF} option.
	\end{itemize}
\item In the \textbf{Topics} field, select \textbf{RTF}:
	\begin{itemize}
	\item Select the \texttt{COMPACT\_RTF} option.
	\end{itemize}
\item Select \textbf{File} $\rightarrow$ \textbf{Save as...} and save the configuration file (the \texttt{/Doc} folder would be a good place for this) leave the name as \texttt{Doxyfile}.
\item In the tabs at the top of the interface select \textbf{Run}:
	\begin{itemize}
	\item Click the \textbf{Run Doxygen} button.
	\item When the output window indicates that Doxygen has finished, Click \textbf{show HTML output} which will open the generated documentation in a browser.
	\item Browse your project and notice how the comments have been built into the documentation.
	\item Navigate to the project \texttt{/Doc} folder and have a look at the files which have been produced.
	\item The output from Doxygen has been saved in the \texttt{/Doc/html} and \texttt{/Doc/rtf} folders.
	\item Double click the file \texttt{/Doc/html/index.html} - this is the root of the html pages.
	\item Right click the file \texttt{/Doc/rtf/refman.rtf} and open the document in word. Many of the fields require updating to display the correct information. So select all (\texttt{cmd} + a) and then right click on the document and select `Update Field.' 
	\item You should generate documentation in this way for your assignment. Use your judgement on what to include as not all of this information will be of interest to the markers.
	\end{itemize}
\end{enumerate}

Have a look through the documentation and notice that class hierarchy diagrams and cross references are generated for all classes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Exercise: Writing Doxygen Commands and Generating Documentation}
Now check that all the classes that you have generated today are well documented.

\begin{enumerate}
\item Ensure that each class has a Doxygen compatible comment above the declaration for the class. Use the \texttt{@see} command to cross reference related classes.
\item For each member function of the class add Doxygen comments describing what the function does and be sure to use the \texttt{@param}, \texttt{@return} commands to describe argument and return types.
\item Generate the documentation for this project and check the output for the chosen class is as expected. 
\item When you are coding in future, write the Doxygen comments as you go to make life easy (it's much harder to write useful comments if you leave this until the end).
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Further Exercises:}

\subsubsection{Exercise: Implementing A Sawtooth Wave Oscillator Using Inheritance}
Extend the application to include a sawtooth oscillator and enable the user to switch between all three oscillators.

\subsubsection{Exercise: Implementing A Triangle Wave Oscillator Using Inheritance}
Extend the application to include a triangle wave oscillator and enable the user to switch between all four oscillators.

Complete the following before next week.
\begin{enumerate}
\item Finish any exercises that were not completed in the lab.

\item Read:
Chapter 13 Ploymorphism, of C++, HOW TO PROGRAM\\
which is available through the library via Safari Books Online at this link: \\
http://proquestcombo.safaribooksonline.com/9780136085669
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Finishing up}

Make sure that all the lines of code that have been produced are suitably recorded in a text file.  Print out the text file, email it, or copy it to the the user Drop Box of any other people who were sharing the computer for this practical such that everyone has an electronic copy.  
