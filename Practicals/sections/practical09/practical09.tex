\rule[.4cm]{\textwidth}{2pt}
\section{Practical 9 - Audio Files, Audio Buffers and Audio Sources in Juce}
\label{sec:prac09}


%%%%%%%%%%%%%%%
In this practical we will focus on the mechanics of audio file reading and writing in Juce through which we will develop an understanding of the \texttt{AudioBuffer} class. We will also use the \texttt{AudioSource} and related/derived classes in Juce and establish how to connect an \texttt{AudioSource} with the standard audio I/O callback we have used in prior sessions. On successful completion of this practical you will have further understanding of: 

\begin{enumerate}
\item Simple audio looping. 
\item How to use and access the Juce \texttt{AudioBuffer} class. 
\item How to read and write between the computer file system and  \texttt{AudioBuffer} objects. 
\item The \texttt{AudioSource} and related classes in Juce. 
\item How to stream audio from files and the \texttt{AudioTransportSource} class. 
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{A Basic Audio Looper}
Take a fork of the \texttt{Practical09a-BasicLooper} repository found at  \url{https://gitlab.uwe.ac.uk/sda} and clone your repository alongside the rest of your SDA projects (remember it must be within the same folder as your Juce repository (but not in the Juce repository)). Build the project and ensure that it works - Watch out for feedback!

\begin{center}
\fbox{\parbox{0.95\textwidth}{\textbf{Please wear headphones for this work as the application is susceptible to feedback.}}}
\end{center}

The project provided this week implements a very simple looper. When the play button is toggled the contents of the loop is repeatedly played through the audio output. If the record button is toggled, the audio microphone input to the computer is written into the looper overwriting anything that was there before. Build, run and test the program and study the code. The important sections will be studied next, which will need to be understood before you attempt the exercises that follow.

\subsubsection{Program Overview}
The program has been divided into classes, their relationship structure is shown below:

\begin{center}
\includegraphics[]{images/struct}
\end{center}

\subsubsection*{\texttt{Looper}}
The \texttt{Looper} class implements the audio looper. Its interface includes functions that set it playing/recording and a \texttt{processSample()} function that returns the next sample from the looper and takes an input value that may be written into the looper, if it were recording. The \texttt{LooperGui} class is a very simple \texttt{Component} class, which includes a reference to the looper so that it can be controlled when the user clicks the buttons on the interface. Notice that the Gui components are completely separated from the audio classes, this is a good programming style that you can use in your own programs. Study \texttt{MainComponent}, \texttt{LooperGui}, \texttt{Audio} and \texttt{Looper} to see how the relationships are set up in the code. 

\subsubsection*{\texttt{Audio}}
The \texttt{Audio} object contains a single instance of the \texttt{Looper} class and also includes a function for retrieving the \texttt{looper} called \texttt{getLooper()}. \texttt{Audio} also manages the audio for the program, if you scroll to the definition of the \texttt{audioDeviceIOCallback()} function in \texttt{Audio.cpp} you will see where the microphone input is fed to the looper and the value returned from the looper is fed to the audio output. 

\subsubsection*{\texttt{MainComponent}}
This class receives and stores a reference to the \texttt{Audio} object and creates a \texttt{LooperGui} object. The constructer of the \texttt{LooperGui} requires a reference to a \texttt{Looper}, which is extracted  from the \texttt{audio} object (via \texttt{getLooper()}). The constructor for \texttt{MainComponent} is as follows:

\begin{verbatim}
MainComponent::MainComponent (Audio& a) 
 : 	audio (a), 
 	  looperGui (audio.getLooper())
\end{verbatim}

\subsubsection*{\texttt{LooperGui}}
The \texttt{LooperGui} is a very simple class with a record button, a play button plus a reference to the \texttt{looper} object, the state of which is set when the buttons are pressed by the user. 

Now navigate to the \texttt{buttonClicked()} callback. When either the play or record buttons are pressed, the looper record or play state is inverted using the not \texttt{!} operator. To keep the GUI up to date with the state of the \texttt{looper}, the toggle state for the relevant button is updated with a call to \texttt{looper.isPlaying()} or \texttt{looper.isRecording()}. 

\subsubsection*{\texttt{Looper}}
The \texttt{Looper} class manages the processes of the audio looper. When playing, the looper repeatedly outputs the contents of its internal audio buffer. This looper also adds (\textit{overdubs}) four metronome clicks for each cycle of the buffer. When recording, the looper \texttt{input} sample is written into the internal buffer, \textit{replacing} anything that was stored previously. 

This \texttt{Looper} is comparable with the delay line effects that we have explored in Audio Process Design and Implementation previously. However, the method here is simplified as we are not using separate read and write positions, a single \texttt{bufferPoisson} variable is used to read and write at the same location. 

The private region of the \texttt{Looper} class includes the following:
\begin{enumerate}
\item two \texttt{Atomic} state variables called \texttt{playState} and \texttt{recordState} to indicate when the application is playing/recording. 
\item a constant called \texttt{bufferSize} that sets the size of the audio buffer (constants can be given values here when declared \texttt{static const}).
\item an array of floating-point variables called \texttt{audioBuffer} in which to store our audio loop.
\item a counter called \texttt{bufferPosition} to keep track of our current position in the \texttt{audioBuffer}.
\end{enumerate}
 
Now, navigate to the \texttt{Looper} constructor in \texttt{Looper.cpp}. Here you will find the data members are initialised: 
\begin{enumerate}
\item The playback and record states are switched off by assigning the \texttt{playState} and \texttt{recordState} \texttt{Atomic} variables to \texttt{false}.
\item The \texttt{bufferPosition} is set to the beginning of the buffer.
\item Each element of the \texttt{audioBuffer} is set to zero.
\end{enumerate}

\textbf{Looper Audio Process} \\
Locate the \texttt{processSample()} function in \texttt{Looper.cpp}. A call to this function is made for each sample that is processed in \texttt{MainComponent::audioDeviceIOCallback()}. The \texttt{processSample()} function operates as follows:

\begin{enumerate}
\item an \texttt{output} variable is created and assigned the value \texttt{0.f}.
\item If play is enabled, the current position of the \texttt{audioBuffer} is assigned to the \texttt{output} variable. 
\begin{verbatim}
        if (playState == true) 
        {
            //play
            output = audioBuffer[bufferPosition];
\end{verbatim}

\item If the \texttt{bufferPosition} is located at the beginning, a quarter, a half or three quarters of the way through the buffer a click is mixed into the output.
\begin{verbatim}
            //click 4 times each bufferLength
            if ((bufferPosition % (bufferSize / 4)) == 0) 
                output += 0.25f;
\end{verbatim}

\item If record is enabled, the current microphone input is written into the \texttt{audioBuffer}. 

\begin{verbatim}
            //record
            if (recordState == true) 
                audioBuffer[bufferPosition] = *inL;
\end{verbatim}

\item Finally the \texttt{bufferPosition} is incremented and if it has reached the end of the buffer it is reset to the beginning.
\begin{verbatim}
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
\end{verbatim}
\end{enumerate}


\subsubsection{Exercise: Extending the Looper}
Having understood the processes described above, extend the looper in three ways:
\begin{enumerate}
\item Currently the looper replaces the previous contents of the \texttt{audioBuffer}  extend the looper such that it overdubs instead.
\item Currently the loop length is four beats in two seconds. Double this such that the loop is eight beats in four seconds. Ensure that there are eight clicks for each loop! 
\item Add another looper (and Gui) to the application with controls in the bottom half of the interface. With the classes arranged as they are this should be as simple as creating a additional \texttt{Looper} and \texttt{LooperGui}, adding them to the interface and mixing their outputs in the audio callback. 
\end{enumerate}

\subsection{The Juce \texttt{AudioBuffer}}
As Juce was originally designed to build audio applications, a convenient class has been provided for the storage of audio data called \texttt{AudioBuffer}. An \texttt{AudioBuffer} can store multiple channels of audio data and provides many useful functions that allow access to, and manipulation of the contained audio. 

\subsubsection{\texttt{AudioBuffer}: Class Info}
The \texttt{AudioBuffer} manages multiple channels of sample arrays. The buffer is much more flexible than a static array as it can be automatically resized at runtime to accommodate any length of audio and any number of channels, managing its internal memory efficiently and reliably. However, you should never attempt to resize the buffer in the audio thread as this can take some time and could result in glitching. 

If you wanted to add an \texttt{AudioBuffer} to the \texttt{Looper} class, you would need to add one to the private section of the class declaration, replacing the \texttt{float} array with the same name:

\begin{verbatim}
  	 AudioBuffer<float> audioBuffer;
\end{verbatim}

In the \texttt{Looper} constructor you should set the required number of channels and the number of samples in each channel, before clearing each element of the buffer. For example:

\begin{verbatim}
   audioBuffer.setSize (1, bufferSize);
   audioBuffer.clear();
\end{verbatim}

This allocates an \texttt{AudioBuffer} with enough space to store one channel of audio with a duration of 2 seconds (assuming a 44.1kHz sample rate), and clears the buffer to ensure that it contains only zeros. Later in the program the individual samples in the buffer can be accessed using the function:

\begin{verbatim}
  	 float* getWritePointer (int channelNumber, int sampleOffset);
\end{verbatim}

which returns a pointer to one of the samples in the buffer at the position specified by second argument (\texttt{sampleOffset}) on the specified (\texttt{channelNumber}). This pointer can be dereferenced to read and write to the buffer. For example, the following code would set the first sample of the single channel stored in \texttt{audioBuffer} to zero:

\begin{verbatim}
   float* audioSample;
   audioSample = audioBuffer.getWritePointer(0, 0);
   *audioSample = 0.0;
\end{verbatim}

\subsubsection{Exercise: An \texttt{AudioBuffer} Looper}

Replace the \texttt{std::array<float, bufferSize> audioBuffer} array within the \texttt{Looper} class with an\\ 
\texttt{AudioBuffer}. Test that the looper works as before.

\subsection{Writing the Contents of an \texttt{AudioBuffer} to an Audio File}
One key advantage of using an \texttt{AudioBuffer} is the ease with which audio data can be written to a wave file. The process is fairly straightforward but involves several new Juce classes. The procedure for doing this is pasted below:

\begin{verbatim}
    FileChooser chooser ("Please select a file...",
    File::getSpecialLocation (File::userDesktopDirectory), "*.wav");
    if (chooser.browseForFileToSave (true))
    {
        File destinationFile (chooser.getResult().withFileExtension (".wav"));
        WavAudioFormat wavFormat;
        std::unique_ptr<AudioFormatWriter> writer (wavFormat.createWriterFor 
                                                  (destinationFile.createOutputStream(),
                                                   44100, 1, 16, nullptr, 0));
        writer->writeFromAudioSampleBuffer (audioBuffer, 0, audioBuffer.getNumSamples());
    }
\end{verbatim}

\subsubsection{Exercise: Save Feature}
Add a save feature to the looper application. Add a save \texttt{TextButton} to the \texttt{LooperGui} class. When this button is pressed, a function called {save()} in the \texttt{Looper} class should execute the code above, you will need to implement this function. You should find the audio in the looper is written to an audio file at the chosen destination. 

To ensure thread safety you should probably check that the looper is not playing while the file is being written. Check the state and only perform the write if the looper is not playing. In the \texttt{LooperGui} class you may wish to check the play state and display a an alert window as follows:

\begin{verbatim}
AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon, 
                    "Error", "Stop playback before trying to save", "OK", this);
\end{verbatim}

\subsection{Reading an Audio File into an \texttt{AudioBuffer}}
It is also quite simple to load an audio file into an \texttt{AudioBuffer} using a similar approach. The procedure is pasted below:

\begin{verbatim}
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    
    FileChooser chooser ("Please select the file you want to load...",
                         File::getSpecialLocation (File::userHomeDirectory),
                         formatManager.getWildcardForAllFormats());
    
    if (chooser.browseForFileToOpen())
    {
        File file (chooser.getResult());
        
        std::unique_ptr<AudioFormatReader> reader 
                                        (formatManager.createReaderFor (destinationFile));
        if (reader != nullptr)
        {
            audioBuffer.setSize (reader->numChannels, (int)reader->lengthInSamples);
            reader->read (&audioBuffer, 0, (int)reader->lengthInSamples, 0, true, false);
        }
    }            
\end{verbatim}

\subsubsection{Exercise: Load Feature}
Add a load feature to the looper application. Add a load \texttt{TextButton} to the \texttt{LooperGui} class. When this button is pressed, a function called {load()} in the \texttt{Looper} class should execute the code above, you will need to implement this function. You should find the audio in the file is loaded into the \texttt{audioBuffer}. 

Again, to ensure thread safety, you should probably check that the looper is not playing while file is being read. Check the state and only perform the read if the looper is not playing. You may wish to again use an alert window as above.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Audio File Streaming}
Take a fork of the \texttt{Practical09b-AudioTransport} repository found at  \url{https://gitlab.uwe.ac.uk/sda}. Clone your repository with the rest of your SDA projects (remember it must be within the same folder as your Juce repository (but not in the Juce repository)). Build the project and ensure that it works. This project implements another file player, only this time the selected audio file is not loaded entirely into a buffer, it is streamed as required from a file. (The audio is still loaded into a buffer, but only up to 32768 samples ahead of the current play position.) 

\subsubsection{Program Overview}
As with the looper application above this program separates the audio from the GUI components but includes a \texttt{FilePlayer} which is responsible for playing the files and a \texttt{FilePlayerGui} which allows the user to control it. The relationship structure is shown below:

\begin{center}
\includegraphics[]{images/struct2}
\end{center}

The \texttt{FilePlayer} class loads and plays the file. Its public member function \texttt{setPlaying()} can begin/stop playback and \texttt{loadFile()} loads the specified audio file into the player. The \texttt{FilePlayerGui} class is a simple \texttt{Component} class, which includes a pointer (not a reference, as with last week's example) to the \texttt{FilePlayer} so that it can be controlled by the user through the interface. 

\subsubsection*{Navigate to the \texttt{MainComponent} class}
The structure of this project is very similar to the looper above but with \texttt{FilePlayer} and \texttt{FilePlayerGui} classes included. Have a look through the code relating the diagram above to what is shown in the code.

\texttt{Audio} again manages the audio for the program but additionally includes a new object of type \texttt{AudioSourcePlayer}, this object actually plays the audio from our \texttt{filePlayer}. In the \texttt{Audio} constructor, you will see that the \texttt{filePlayer} object is loaded into the \texttt{audioSourcePlayer} with the code:

\begin{verbatim}
//set the filePlayer as the audio source 
audioSourcePlayer.setSource (&filePlayer);  
\end{verbatim}

Audio samples are then pulled from the \texttt{audioSourcePlayer} in the \texttt{audioDeviceIOCallback()} with the call:

\begin{verbatim}
audioSourcePlayer.audioDeviceIOCallback (inputChannelData, 
                                         numInputChannels, 
                                         outputChannelData, 
                                         numOutputChannels, 
                                         numSamples);
\end{verbatim}

The \texttt{audioSourcePlayer} then draws samples from its source (the \texttt{filePlayer}) and they are streamed to the audio output. Note that the \texttt{audioSourcePlayer} is also prepared for playback in the\\ \texttt{audioDeviceAboutToStart()} and \texttt{audioDeviceStopped()} functions.

A representation of the audio object connections is illustrated below, these blocks will be explained in this week's lecture:

\begin{center}
\includegraphics[]{images/startConnection}
\end{center}

\subsubsection*{Navigate to the \texttt{FilePlayerGui} class}
The \texttt{FilePlayerGui} is a simple class with a play/stop button, a file selector and a pointer to a \texttt{FilePlayer} object, the state of which is set when the button/file components are manipulated by the user. 

Now navigate to the \texttt{buttonClicked()} callback. Whenever the the play button is pressed, the playback state is inverted. Now find the \texttt{filenameComponentChanged()} function. Here the file that the user selects is extracted and, if it exists, the \texttt{filePlayer} is instructed to load it. 

\subsubsection*{Navigate to the \texttt{FilePlayer} class}
The \texttt{FilePlayer} is an example of an \texttt{AudioSource} (this is why it is played using an \texttt{AudioSourcePlayer}), note that it inherits from \texttt{AudioSource} and implements the associated functions: \texttt{prepareToPlay(), getNextAudioBlock(), releaseResources()}. The audio data is read and streamed from the selected file using an \texttt{AudioTransportSource} object.
When the \texttt{FilePlayer} play state is switched in \texttt{setPlaying()} the functions \texttt{start()}, \texttt{stop()} and \texttt{setPosition (0.0)} are called on the \\\texttt{AudioTransportSource} object.

\subsubsection{Exercise: Playback Position Control}
Extend the program such that there is a horizontal slider on the interface which is used to control the current playback position within the file. To achieve this:

\begin{enumerate}
\item Create, add and position a slider on the \texttt{FilePlayerGui}. The slider should have a range of 0.0 - 1.0. Don't forget to make \texttt{FilePlayerGui} a \texttt{Slider::Listener}.
\item In the \texttt{sliderValueChanged()} callback, a function called \texttt{setPosition (double newPosition)} should be called on the \texttt{FilePlayer} object and the value of the slider should be used as the function's argument. You will now need to implement this function in the \texttt{FilePlayer} class. 
\item In the \texttt{FilePlayer} class declare and define the \texttt{setPosition()} function and set the transport position by calling the function \texttt{setPosition (double newPosition)} on the  \texttt{AudioTransportSource} object. The units of the new position are in seconds and the length of the file can be obtained by calling \texttt{getLengthInSeconds()} on the \texttt{audioTransportSource} object.
\end{enumerate}

Extend this exercise such that the position of the slider stays in sync with the current play position. To do this set up a quarter second timer (look up the class \texttt{Timer}) in \texttt{FilePlayerGui} which starts when the \texttt{playButton} is clicked and the \texttt{filePlayer isPlaying()} returns \texttt{true} and stops when the \texttt{playButton} is clicked and the \texttt{filePlayer isPlaying()} function returns \texttt{false}. In the \texttt{FilePlayer} class implement a function called \texttt{getPosition()} which returns the current position of the \texttt{AudioTransportSource} and updates the position of the slider accordingly (you will need to divide this value by the file length in seconds to scale it to the range 0.0 - 1.0). 

\subsection{Exercise: Adjusting Playback Speed}
Juce has a selection of classes that are compatible with any class derived from \texttt{AudioSource}, like our \texttt{FilePlayer}. For example, a \texttt{ResamplingAudioSource} can be used to adjust the playback rate of our \texttt{FilePlayer}. Within \texttt{FilePlayer} a \texttt{ResamplingAudioSource} can be connected to the \texttt{AudioTransportSource}. This modification is represented in the diagram below:

\begin{center}
\includegraphics[]{images/resample}
\end{center}

Expand the Previous exercise by inserting a \texttt{ResamplingAudioSource} to control the playback rate of the \texttt{AudioTransportSource}.

\begin{enumerate}
\item Add a \texttt{pitchSlider} for pitch control to the \texttt{FilePlayerGui}, set its range to 0.01 - 5.0 and an initial value of 1.0 using \texttt{setValue()}. A playback rate of 0.00 will cause an assertion error.
\item When the \texttt{pitchSlider} is changed, a function called \texttt{setPlaybackRate (double newRate)} should be called on the \texttt{FilePlayer} object  where the \texttt{newRate} should be the value of the slider. You will now need to implement the \texttt{setPlaybackRate()} function in the \texttt{FilePlayer}.
\item Add a unique pointer to the \texttt{ResamplingAudioSource} class called \texttt{resampingAudioSource} and, in the constructor, create a \texttt{ResamplingAudioSource} object with the following call:
\begin{verbatim}
resamplingAudioSource = std::make_unique<ResamplingAudioSource> 
                        (&audioTransportSource, false);
\end{verbatim}
which will set our \texttt{ResamplingAudioSource} to play and resample the \texttt{audioTransportSource}.
\item Remember to \texttt{delete} the \texttt{ResamplingAudioSource} object in the destructor.
\item We will now use the \texttt{resamplingAudioSource} to play the \texttt{audioTransportSource}. In the\\ \texttt{prepareToPlay(), getNextAudioBlock() and releaseResources() } functions, replace all references to \texttt{audioTransportSource} with \texttt{resamplingAudioSource}.
\item Now, also in \texttt{FilePlayer}, implement the \texttt{setPlaybackRate (double newRate)} such that the function \texttt{setResamplingRatio()} is called on the \texttt{ResampingAudioSource} object with the value of \texttt{newValue} as its argument.
\end{enumerate}

Run the program and you should have pitch control.

\subsubsection{Exercise: Adding Multiple \texttt{FilePlayers}}
In this exercise you are to add additional players, but in a scalable way. Rather then just creating a new \texttt{FilePlayer} and \texttt{FilePlayerGui}, we will use arrays with a constant that controls the quantity of players such that the number of players is easily controlled and loops are used to control the implementation, such that the quantity of players can be controlled by changing only the value of the constant. To complete this exercise: 

%Change this to declare a constant in Audio not MainComponent
\begin{enumerate}
\item Open the file \texttt{Audio.h} and in the public section declare an enumeration contaning one member called \texttt{NumberOfFilePlayers} - set this to be 2 for now. Note that this value can then be accessed anywhere that includes \texttt{Audio.h} with the statement \texttt{Audio::NumberOfFilePlayers}.
\begin{verbatim}
    enum
    {
        NumberOfFilePlayers = 2
    };
\end{verbatim}
\item Also in \texttt{Audio.h} convert the single \texttt{FilePlayer} to a \texttt{std::array} of size \texttt{NumFilePlayers}. Do the same for the \texttt{FilePlayerGui}. You will need to add an index to the function \texttt{Audio::getFilePlayer()} so that the file players can be accessed individually.  
\item In the \texttt{MainComponent} constructor use a loop to pass the address of each \texttt{filePlayer} into its respective \texttt{FilePlayerGui} and to make each \texttt{FilePlayerGui} visible. 
\item Also in the \texttt{MainComponent} constructor, add only the first \texttt{filePlayer[0]} as the \texttt{audioSourcePlayer}'s source.
\item In \texttt{resized()} use a loop to position the player GUIs one beneath the other.
\end{enumerate}

Build the program and test it - you will find that only the first player will play as it is the only player connected with the \texttt{audioSourcePlayer}. This is represented by the audio object connections shown below:

\begin{center}
\includegraphics[width=\textwidth]{images/nomix}
\end{center}

To hear all the \texttt{Fileplayer}s, we need to mix their outputs. Fortunately, there is a \texttt{MixerAudioSource} class, which is designed to mix the outputs from any number of \texttt{AudioSources}. To hear both of the \texttt{FilePlayer} objects, they should be connected to a \texttt{MixerAudioSource} and then use the \texttt{AudioSourcePlayer} to to play the mixer. This updated representation of the audio object connections is illustrated below:

\begin{center}
\includegraphics[width=\textwidth]{images/mix}
\end{center}

%change this to put the audio stuff in Audio (not MainComponent)
To do this:
\begin{enumerate}
\item Declare a \texttt{MixerAudioSource} object in the private section of \texttt{Audio}.
\item In the \texttt{Audio()} constructor use a loop to add each \texttt{filePlayer} to the \texttt{MixerAudioSource} object using the function \texttt{addInputSource()}, the second argument should be \texttt{false} (i.e. the fileplayers should not be deleted by the mixer as they are in a static array).
\item In the \texttt{$\sim$Audio()} destructor, remove all inputs from the mixer by calling \texttt{removeAllInputs()} on the \texttt{MixerAudioSource} object.
\item Now, back in the constructor add your \texttt{MixerAudioSource} object to the your \texttt{audioSourcePlayer}. 
\end{enumerate}

\subsection{Exercise: Accessing the Audio}

In the \texttt{Audio::audioDeviceIOCallback()}, the audio is drawn from the \texttt{audioSourcePlayer} with the call \texttt{audioSourcePlayer.audioDeviceIOCallback()}. Following this call the audio that the \texttt{MixerAudioSource} object has pulled from the file players can be accessed in the \texttt{outputChannelData} array. The audio can then be accessed, sample by sample in a similar style to the \texttt{AudioProcessing} plugin project. At the moment the audio is passed through at unity gain. 

Add a master gain control slider to control the overall gain from the application.

\subsubsection{Unguided Further Exercise: Individual \texttt{FilePlayer} Gain Control}

It would be useful to add individual gain controls to the \texttt{FilePlay} and \texttt{FilePlayerGui} classes. Add this functionality - you will have to read about the \texttt{AudioSourceChannelInfo} class to implement this.

\subsection{Further Extensions}
Spend some time extending the looper application further as you would like. Some suggestions are listed below:

\begin{enumerate}
\item Allow the looper to be controlled by MIDI such that it records when a note is pressed and stops recording when it is released.
\item Add a reverse button such that the looper plays backwards.
\item Add variable speed playback such that the loop can speed up and slow down.
\item Use a \texttt{FilenameComponent} on the GUI to enable the file to be easily loaded.
\end{enumerate}



%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
