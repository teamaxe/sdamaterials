//
//  Array.h
//  CommandLineTool
//
//  Created by Tom Mitchell on 07/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef H_ARRAY
#define H_ARRAY

/**
 Class that manages a dynamic array of floats
 */
class Array
{
public:
    /** Constructor */
    Array();
    
    /** Destructor */
    ~Array();
    
    /** 
     Returns the size of the array - the number of elements stored in the array 
     @returns       the size of the array
     */
    int size() const;
    
    /** 
     adds the @value argument as a new item at the end of the array 
     @param newValue    value to be added to the end of the array
     */
    void add (float newValue);
    
    /** 
     returns the item in the array at @index
     @param index    the item index in the array that should be returned
     @returns       the value of the item at @index
     */
    float get (int index) const;
    
    /** 
     removes the item at @index
     @param index    the item index in the array that should be removed
     */
    void remove (int index);
    
    /** 
     Reverses the array using an iterative method 
     */
    void reverse();
    
    /**
     Function that tests this class to make sure everything works.
     @returns   True if successful
     */
    static bool testArray();
private:
    int arraySize;
    float* data;
};

#endif /* H_ARRAY */
