/*
 *  SinOscillator.cpp
 *  Software Development for Audio
 */


#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

/** Class for a sinewave oscillator */

class SinOscillator  
{
public:
	/** SinOscillator constructor */
	SinOscillator();
	
	/** SinOscillator destructor */
	~SinOscillator();
	
	/** sets the frequency of the oscillator */
	void setFrequency (float freq);
	
	/** sets frequency using a midi note number */
	void setNote (int noteNum);
	
	/** sets the amplitude of the oscillator */
	void setAmplitude (float amp);
	
	/** resets the oscillator */
	void reset();
	
	/** sets the sample rate */
	void setSampleRate (float sr);
	
	/** returns the next sample */
	float nextSample();
	
	/** function that provides the execution of the waveshape */
	float renderWaveShape (float currentPhase);
	
private:
	float frequency {440.f};
    float amplitude {0.f};
    float sampleRate {44100.f};
	float phase {0.f};
    float phaseInc {0.f};
};
